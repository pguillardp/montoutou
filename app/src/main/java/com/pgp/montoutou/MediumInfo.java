package com.pgp.montoutou;

import android.graphics.Bitmap;
import android.net.Uri;

import java.util.Date;

public class MediumInfo {
    private int orientation;
    private Uri thumbnail;
    private Bitmap thumbBitmap;
    private Uri fullscreen;
    private Bitmap fullscreenBitmap;
    private Date dateTaken = new Date();
    private String data = "";
    private String mime = "";

    public Uri getUri() {
        return Uri.parse(data);
    }

    public int getOrientation() {
        return orientation;
    }

    public void setOrientation(int orientation) {
        this.orientation = orientation;
    }

    public Uri getThumbnail() {
        return thumbnail;
    }

    public void setThumbnail(Uri thumbnail) {
        this.thumbnail = thumbnail;
    }

    public Bitmap getThumbBitmap() {
        return thumbBitmap;
    }

    public void setThumbBitmap(Bitmap thumbBitmap) {
        this.thumbBitmap = thumbBitmap;
    }

    public Uri getFullscreen() {
        return fullscreen;
    }

    public void setFullscreen(Uri fullscreen) {
        this.fullscreen = fullscreen;
    }

    public Bitmap getFullscreenBitmap() {
        return fullscreenBitmap;
    }

    public void setFullscreenBitmap(Bitmap fullscreenBitmap) {
        this.fullscreenBitmap = fullscreenBitmap;
    }

    public Date getDateTaken() {
        return dateTaken;
    }

    public void setDateTaken(Date date) {
        this.dateTaken = date;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public String getMime() {
        return mime;
    }

    public String getExtension() {
        String[] fs = data.split("\\.");
        return fs.length > 0 ? fs[fs.length - 1] : "";
    }

    public void setMime(String mime) {
        this.mime = mime;
    }

    public boolean isVideo() {
        return getMime().startsWith("video");
    }

    public boolean isImage() {
        return getMime().startsWith("image");
    }

}
