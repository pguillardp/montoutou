package com.pgp.montoutou;

import android.content.DialogInterface;
import android.widget.TextView;
import androidx.appcompat.app.AlertDialog;
import com.pgp.montoutou.ui.main.DataCore;
import com.pgp.montoutou.ui.main.SharedViewModel;
import com.pgp.montoutou.ui.main.logs.DataLog;
import com.pgp.montoutou.ui.main.settings.DataSettings;
import org.apache.commons.net.ftp.FTP;
import org.apache.commons.net.ftp.FTPFile;
import org.apache.commons.net.ftp.FTPFileFilter;
import org.apache.commons.net.ftp.FTPSClient;
import org.jetbrains.annotations.Nullable;
import org.json.JSONObject;

import java.io.*;
import java.net.InetAddress;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class Synchro {
    public static final String DATE_FORMAT = "yyyyMMddHHmmss";
    private static FTPSClient ftpClient = new FTPSClient();
    private static String dogName;
    private static final Map<String, FTPFile> remotemap = new HashMap<>();
    private static final Map<String, FTPFile> remotesyncmap = new HashMap<>();
    private static final Map<String, FTPFile> remoteSyncmedia = new HashMap<>();
    private static final Map<String, String> localmap = new HashMap<>();
    private static SharedViewModel sharedViewModel;
    private static Thread task = null;
    private static boolean connecting = false;
    private static boolean full = false;

    private static TextView testStatus;

    private static boolean isConnectionTest() {
        return testStatus != null;
    }


    public static void testConnection(DataSettings settings, TextView testResultView) {
        synchronizeAsync(null, settings, "", testResultView, false);
    }


    public static void synchronizeAsync(SharedViewModel sharedViewModel, DataSettings settings, String dogName, DataSettings dataSettings) {
        synchronizeAsync(sharedViewModel, dataSettings, Utils.getInstance().getDogName(), null, false);
    }

    public static void synchronizeAsync(SharedViewModel sharedViewModel, DataSettings dataSettings) {
        synchronizeAsync(sharedViewModel, dataSettings, Utils.getInstance().getDogName(), null, false);
    }

    public static void synchronizeFullAsync(SharedViewModel sharedViewModel, DataSettings dataSettings) {
        synchronizeAsync(sharedViewModel, dataSettings, Utils.getInstance().getDogName(), null, true);
    }

    private static void synchronizeAsync(SharedViewModel sharedViewModel, DataSettings settings, String dogName, TextView testResultView, boolean full) {
        Synchro.sharedViewModel = sharedViewModel;
        Synchro.testStatus = testResultView;
        Synchro.full = full;

        if (ftpClient.isConnected()) {
            try {
                connecting = false;
                ftpClient.disconnect();
                task.stop();
                task = null;
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                updateGuiSync("", "");
                updateConnected();
                return;
            }
        }

        task = new Thread(new Runnable() {
            public void run() {
                try {
                    Synchro.synchronize(sharedViewModel, settings, dogName);
                } catch (Exception e) {
                    System.out.println(e.getMessage());
                    if (ftpClient.isConnected()) {
                        try {
                            connecting = false;
                            ftpClient.disconnect();
                            updateConnected();
                            updateGuiSync("", "");
                            if (!Synchro.isConnectionTest())
                                alertMessage("Synchronisation interrompue: "
                                        + e.getMessage());
                        } catch (IOException ex) {
                            ex.printStackTrace();
                        }
                    }
                } finally {
                    updateConnected();
                }
            }
        });
        task.start();
    }

    private static void updateGuiSync(String status, String message) {

        runOnMainThread(new Runnable() {
            @Override
            public void run() {
                // test?
                if (Synchro.isConnectionTest()) {
                    Synchro.testStatus.setText(message);
                    return;
                }
                MainActivity.getInstance().updateSync(status, message);
            }//public void run() {
        });
    }

    private static void runOnMainThread(Runnable run) {
        MainActivity.getInstance().runOnUiThread(run);
    }


    private static void updateConnected() {
        runOnMainThread(new Runnable() {
            @Override
            public void run() {
                String message = "";
                if (connecting)
                    message = "connexion en cours...";
                else if (ftpClient.isConnected())
                    message = "stopper la synchronisation";
                else
                    message = "synchroniser";
                MainActivity.getInstance().updateConnected(message);
            }
        });
    }


    private static void alertMessage(String message) {
        runOnMainThread(new Runnable() {
            @Override
            public void run() {
                AlertDialog alertDialog = new AlertDialog.Builder(MainActivity.getInstance()).create();
                alertDialog.setTitle(Utils.NOM_APP);
                alertDialog.setMessage(message);
                alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "Ok",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        });
                alertDialog.show();
            }
        });
    }


    private static void synchronize(SharedViewModel sharedViewModel, DataSettings settings, String dogName) throws IOException, ParseException {

        if (ftpClient.isConnected()) {
            if (isConnectionTest())
                ftpClient.disconnect();
            else {
                alertMessage("Une synchronisation est déja en cours...");
                return;
            }
        }

        // open ftp
        connecting = true;
        updateConnected();
        updateGuiSync("connect", "connect");
        ftpClient = new FTPSClient("tls");
        ftpClient.setConnectTimeout(10000);
        InetAddress byName = InetAddress.getByName(settings.getRepoUrl());
        ftpClient.enterLocalPassiveMode();
        ftpClient.connect(byName);
        ftpClient.enterLocalPassiveMode();
        updateGuiSync("connect", "login");
        ftpClient.login(settings.getLogin(), settings.getPassword());
        ftpClient.enterLocalPassiveMode();
        connecting = false;
        updateConnected();
        updateGuiSync("connect", "ok");

        if (Synchro.isConnectionTest()) {
            ftpClient.logout();
            ftpClient.disconnect();
            updateGuiSync("connect", "connexion réussie");
            return;
        }

        Synchro.dogName = dogName;

        // create local & remote sync folders if needed
        File file = getSyncFolders();
        if (file == null)
            return;

        // sync deleted files
        syncDeletedFiles();

        // download/upload modified files
        syncModifiedFiles();

        ftpClient.logout();
        ftpClient.disconnect();
    }


    public static String getAbsoluteLocalFilePath(String fname, boolean sync) {
        String path = Utils.getInstance().getDogFolser(dogName).getAbsolutePath()
                + (sync ? "/sync" : "")
                + ("".equals(fname) ? "" : ("/" + fname));
        return path;
    }


    public static String getRemoteFilePath(String fname, boolean sync) {
        String parh = "/" + dogName
                + (sync ? "/sync" : "")
                + ("".equals(fname) ? "" : ("/" + fname));
        return parh;
    }


    private static void syncDeletedFiles() throws IOException {

        updateGuiSync("delete", "delete");

        // local to server
        File fd = new File(getAbsoluteLocalFilePath("", true));
        String[] fs = fd.list();

        for (String fname : fs) {
            updateGuiSync("delete", "delete");

            if (!fname.endsWith(".json"))
                continue;

            FTPFile[] remoteFiles = ftpClient.listFiles(getRemoteFilePath(fname, false));
            if (remoteFiles.length > 0)
                ftpClient.deleteFile(getRemoteFilePath(fname, false));

            File uploadFile = new File(getAbsoluteLocalFilePath(fname, true));
            ftpClient.changeWorkingDirectory(getRemoteFilePath("", true));
            uploadFile(uploadFile, fname, null);

            File f = new File(getAbsoluteLocalFilePath(fname, true));
            f.delete();

            f = new File(getAbsoluteLocalFilePath(fname, false));
            if (f.exists())
                f.delete();

            updateGuiSync("", "");
        }

        // server to local
        String remotesync = getRemoteFilePath("", true);
        ftpClient.changeWorkingDirectory(remotesync);
        updateGuiSync("delete", "delete");
        FTPFile[] result = ftpClient.listFiles(remotesync, new FTPFileFilter() {
            @Override
            public boolean accept(FTPFile ftpFile) {
                return ftpFile.isFile()
                        && ftpFile.getName().endsWith(".json");
            }
        });
        boolean refresh = false;
        for (int i = 0; i < result.length; i++) {
            updateGuiSync("delete", "delete");
            File f = new File(getAbsoluteLocalFilePath(result[i].getName(), false));
            if (f.exists()) {
                f.delete();
                refresh = true;
            }
            if (refresh)
                runOnMainThread(new Runnable() {
                    @Override
                    public void run() {
                        Synchro.sharedViewModel.readLearnings();
                        Synchro.sharedViewModel.readLogs();
                    }
                });
            updateGuiSync("", "");
        }
    }


    private static void syncModifiedFiles() throws IOException, ParseException {
        ftpClient.changeWorkingDirectory(getRemoteFilePath("", false));

        FTPFile[] rfs = ftpClient.listFiles();
        remotemap.clear();
        for (FTPFile f : rfs) {
            if (f.getName().startsWith("..") || f.isDirectory())
                continue;
            remotemap.put(f.getName(), f);
        }

        File f = new File(getAbsoluteLocalFilePath("", false));
        String[] fs = f.list();
        localmap.clear();
        if (fs != null) {
            for (String s : fs) {
                if (!s.contains("sync")
                        && !s.contains("photos"))
                    localmap.put(s, "");
            }
        }

        boolean update = false;
        for (Map.Entry<String, FTPFile> entry : remotemap.entrySet()) {
            updateGuiSync("download", "download");
            update |= downloadIfModified(entry.getKey());
            updateGuiSync("", "");
        }
        if (update) {
            runOnMainThread(new Runnable() {
                @Override
                public void run() {
                    Synchro.sharedViewModel.notifyLogs();
                }//public void run() {
            });
        }

        for (Map.Entry<String, String> entry : localmap.entrySet()) {
            updateGuiSync("upload", "upload");
            uploadIfModified(entry.getKey());
            updateGuiSync("", "");
        }
        updateGuiSync("", "");
    }


    private static boolean downloadIfModified(String remoteFileName) throws IOException, ParseException {

        ftpClient.changeWorkingDirectory(getRemoteFilePath("", false));

        Date remoteDate = getRemoteFileDate(remoteFileName); // ftpFile.getTimestamp().getTime();
        if (!isDownloadable(remoteFileName, remoteDate))
            return false;

        // downmload image if any
        String[] fs = remoteFileName.split("\\.");
        String remotesync = getRemoteFilePath("", true);
        ftpClient.changeWorkingDirectory(remotesync);
        FTPFile[] result = ftpClient.listFiles(remotesync, new FTPFileFilter() {
            @Override
            public boolean accept(FTPFile ftpFile) {
                return ftpFile.isFile()
                        && !ftpFile.getName().endsWith("date")
                        && !ftpFile.getName().contains(Utils.IDENT)
                        && ftpFile.getName().startsWith(fs[0]);
            }
        });

        boolean success = false;
        String _id = "";
        String photoName = "";
        if (result.length > 0) {
            String remoteFilePath = remotesync + "/" + result[0].getName();
            String mediumFileName = Utils.fileNameToMediumName(result[0].getName());
            photoName = mediumFileName;
            String mediumFilePath = Utils.getInstance().getDogPhotoFolder().toString() + "/" + mediumFileName;
            File downloadFile = new File(mediumFilePath);
            BufferedOutputStream outputStream1 = new BufferedOutputStream(new FileOutputStream(downloadFile));

            ftpClient.setFileType(FTP.BINARY_FILE_TYPE);
            success = ftpClient.retrieveFile(remoteFilePath, outputStream1);
            outputStream1.close();

            if (!success)
                return success;

            // insert into album
            _id = Utils.addToPictureOrVideoAlbum(mediumFilePath);
        }

        ftpClient.setFileType(FTP.ASCII_FILE_TYPE);

        DataCore dataCore;
        File downloadFile = new File(getAbsoluteLocalFilePath(remoteFileName, false));
        String prev_id = "";
        if (downloadFile.exists()) {
            dataCore = DataCore.fromFile(downloadFile.getName());
            prev_id = dataCore.get_id();
        }
        if (!"".equals(_id))
            prev_id = _id;

        ftpClient.changeWorkingDirectory(getRemoteFilePath("", false));
        downloadFile.createNewFile();
        OutputStream outputStream1 = new BufferedOutputStream(new FileOutputStream(downloadFile.getAbsolutePath()));
        String remoteFilePath = getRemoteFilePath(remoteFileName, false);
        success = ftpClient.retrieveFile(remoteFilePath, outputStream1);
        outputStream1.close();

        if (!success)
            return success;

        dataCore = DataCore.fromFile(downloadFile.getName());
        dataCore.set_id(prev_id);
        dataCore.setPhotoName((photoName));
        dataCore.save();
        dataCore.setFileModifiedDate(remoteDate.getTime());

        // new ident ?
        if (remoteFileName.contains(Utils.IDENT)) {
            runOnMainThread(new Runnable() {
                @Override
                public void run() {
                    MainActivity.getInstance().updateIdent(remoteFileName);
                }
            });

        } else {
            final String fname = dataCore.getFileName();
            runOnMainThread(new Runnable() {
                @Override
                public void run() {
                    MainActivity.getInstance().updateDataCore(fname);
                }
            });
        }

        return success;
    }


    private static boolean isDownloadable(String remoteFileName, Date remoteDate) {

        if (remoteFileName.contains(Utils.SETTINGS)) {
            for (String k : localmap.keySet()) {
                if (k.contains(Utils.SETTINGS) && !k.equals(remoteFileName))
                    return false;
            }
        }

        // full sync
        if (full) {
            DataCore dc = DataCore.fromFile(remoteFileName);
            if (dc instanceof DataLog) {
                DataLog log = (DataLog) dc;
                if (log.hasData()
                        && !log.isDataAccessible(log.get_id()))
                    return true;
            }
        }

        Date localDate = new Date();
        if (localmap.containsKey(remoteFileName)) {
            return remoteDate.compareTo(localDate) >= 0;
        }

        return true;
    }


    private static void uploadIfModified(String localFile) throws IOException, ParseException {

        String remoteFile = getRemoteFilePath(localFile, false);
        File fl = new File(getAbsoluteLocalFilePath(localFile, false));

        Date localDate = new Date(fl.lastModified());
        if (!isUploadable(localFile, remoteFile, localDate))
            return;

        // upload medium if any
        boolean success = false;
        MediumInfo info = getMediumInfo(fl.getName());
        if (info.getData() != null && !"".equals(info.getData())) {
            String filenoext = getFileNameWithoutExt(localFile);
            String remoteMediumFile = getRemoteFilePath(filenoext + "." + info.getExtension(), true);
            localFile = info.getData();
            File fm = new File(localFile);
            ftpClient.setFileType(FTP.BINARY_FILE_TYPE);
            success = uploadFile(fm, remoteMediumFile, null);
            if (success)
                System.out.println(localFile + " uploaded successfully.");
            else
                return;
        }

        ftpClient.setFileType(FTP.ASCII_FILE_TYPE);

        success = uploadFile(fl, remoteFile, localDate);
        if (success)
            System.out.println(localFile + " uploaded successfully.");
    }

    private static boolean isUploadable(String localFile, String remoteFile, Date localDate) throws IOException, ParseException {
        if (localFile.contains(Utils.IDENT)) {
            for (String k : remotemap.keySet()) {
                if (k.contains(Utils.IDENT) && !k.equals(localFile))
                    return false;
            }
        }
        if (localFile.contains(Utils.SETTINGS)) {
            for (String k : remotemap.keySet()) {
                if (k.contains(Utils.SETTINGS) && !k.equals(localFile))
                    return false;
            }
        }
        if (remotemap.containsKey(localFile)) {
            Date remoteDate = getRemoteFileDate(remoteFile); //.getTimestamp().getTime();
            return localDate.compareTo(remoteDate) > 0;
        }
        return true;
    }

    private static MediumInfo getMediumInfo(String fulllocal) {
        JSONObject json = Utils.getInstance().jsonFileContent(fulllocal);
        DataLog log = new DataLog();
        log.fromJson(json);
        String[] f = fulllocal.split("/");
        log.setFileName(f[f.length - 1]);
        if (log.getPhoto() == null
                || log.getPhoto().toString().equals(""))
            return new MediumInfo();

        return Utils.getInstance().getMediumInfo(MainActivity.getInstance(), log.get_id());
    }


    @Nullable
    private static File getSyncFolders() throws IOException {
        // default sync folder
        File file = new File(getAbsoluteLocalFilePath("", false));
        if (!file.exists()) {
            file.mkdir();
            file = new File(getAbsoluteLocalFilePath("", true));
            file.mkdir();
        }

        FTPFile[] rdirs = ftpClient.listDirectories();
        boolean found = false;
        for (int i = 0; i < rdirs.length; i++) {
            if (rdirs[i].isDirectory()
                    && rdirs[i].getName().equals(dogName)) {
                found = true;
                break;
            }
        }
        if (!found) {
            ftpClient.makeDirectory(getRemoteFilePath("", false));
            ftpClient.changeWorkingDirectory(dogName);
            ftpClient.makeDirectory(getRemoteFilePath("", true));
        }
        return file;
    }


    private static boolean uploadFile(File uploadFile, String remoteFile, Date localDate) throws IOException {
        InputStream inputStream = new FileInputStream(uploadFile);
        boolean done = ftpClient.storeFile(remoteFile, inputStream);
        inputStream.close();
        if (localDate != null && done) {
            setRemotefileDate(uploadFile.getName(), localDate);
        }

        return done;
    }

    private static void setRemotefileDate(String remote, Date date) throws IOException {
        String datefile = getFileNameWithoutExt(remote) + ".date";
        datefile = getRemoteFilePath(datefile, true);

        File uploadfile = new File(getAbsoluteLocalFilePath("dummy", true));
        if (!uploadfile.exists())
            uploadfile.createNewFile();

        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(DATE_FORMAT);
        String timeVal = simpleDateFormat.format(date);

        Utils.getInstance().writeTextData(uploadfile, timeVal);

        uploadfile = new File(getAbsoluteLocalFilePath("dummy", true));
        uploadFile(uploadfile, datefile, null);
    }

    private static Date getRemoteFileDate(String remote) throws IOException, ParseException {
        String datefile = getFileNameWithoutExt(remote) + ".date";
        String[] fs = datefile.split("/");
        datefile = fs[fs.length - 1];
        datefile = getRemoteFilePath(datefile, true);

        File downloadFile = new File(getAbsoluteLocalFilePath("dummy", true));
        if (!downloadFile.exists())
            downloadFile.createNewFile();

        ftpClient.setFileType(FTP.ASCII_FILE_TYPE);
        OutputStream outputStream = new BufferedOutputStream(new FileOutputStream(downloadFile.getAbsolutePath()));
        boolean success = ftpClient.retrieveFile(datefile, outputStream);
        outputStream.close();

        Date date = new Date();
        if (success) {
            downloadFile = new File(getAbsoluteLocalFilePath("dummy", true));
            String content = Utils.getInstance().getdata(downloadFile);
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat(DATE_FORMAT);
            date = simpleDateFormat.parse(content);

        } else {
            setRemotefileDate(remote, date);
        }
        return date;
    }

    private static String getFileNameWithoutExt(String name) {
        int i = name.lastIndexOf('.');
        String filenoext = name.substring(0, i);
        return filenoext;
    }

}
