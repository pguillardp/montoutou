package com.pgp.montoutou.ui.main.logs;

import android.content.Context;
import android.graphics.Typeface;
import android.os.Bundle;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.*;
import androidx.annotation.NonNull;
import androidx.lifecycle.ViewModelProvider;
import com.pgp.montoutou.MainActivity;
import com.pgp.montoutou.MediumInfo;
import com.pgp.montoutou.R;
import com.pgp.montoutou.Utils;
import com.pgp.montoutou.databinding.FragmentMainBinding;
import com.pgp.montoutou.ui.main.DataCore;
import com.pgp.montoutou.ui.main.DoggyFragment;
import com.pgp.montoutou.ui.main.SharedViewModel;
import com.pgp.montoutou.ui.main.learnings.DataProgress;

import java.util.ArrayList;
import java.util.Collections;

/**
 * A placeholder fragment containing a simple view.
 */
public class LogsFragment extends DoggyFragment {

    private static final String ARG_SECTION_NUMBER = "section_number";
    private SharedViewModel sharedViewModel;
    private FragmentMainBinding binding;
    private LogAdapter adapter;
    private final ArrayList<DataCore> dataCores = new ArrayList<>();
    private int selectedNote = -1;
    private ViewGroup viewGroup;

    public static LogsFragment newInstance(int index) {
        LogsFragment fragment = new LogsFragment();
        Bundle bundle = new Bundle();
        bundle.putInt(ARG_SECTION_NUMBER, index);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        sharedViewModel = new ViewModelProvider(MainActivity.getInstance()).get(SharedViewModel.class);
        int index = 1;
        if (getArguments() != null) {
            index = getArguments().getInt(ARG_SECTION_NUMBER);
        }
        sharedViewModel.setIndex(index);
        sharedViewModel.getLogsLive().observe(this, logs -> {
            if (logs != null)
                updateAllLogTypes();
        });
        sharedViewModel.getProgressLive().observe(this, dataProgresses -> {
            if (dataProgresses != null) updateAllLogTypes();
        });
    }


    @Override
    public View onCreateView(
            @NonNull LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {

        binding = FragmentMainBinding.inflate(inflater, container, false);
        View logvw = inflater.inflate(R.layout.fragment_notes_page, container, false);
        ListView listView = logvw.findViewById(R.id.logView);
        updateAllLogTypes();
        adapter = new LogAdapter(getActivity(), this, dataCores);
        listView.setAdapter(adapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> arg0, View view, int position, long arg3) {
                selectedNote = position;
            }
        });

        return logvw;
    }


    private void updateAllLogTypes() {
        dataCores.clear();
        if (adapter != null) {
            adapter.clear();
            adapter.notifyDataSetChanged();
        }
        for (DataLog log : sharedViewModel.getLogsLive().getValue())
            dataCores.add(log);
        for (DataProgress progress : sharedViewModel.getProgress())
            dataCores.add(progress);
        sortAdapter();
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
    }


    @Override
    public void selectDog(String dogName) {
        sharedViewModel.readLogs();
        sharedViewModel.readLearnings();
        sharedViewModel.computeProgress();
        sharedViewModel.notifyLogs();
    }


    public void onAdd() {
        DataLog log = new DataLog();
        log.save();
        sharedViewModel.getLogsLive().getValue().add(log);
        sharedViewModel.notifyLogs();
    }

    @Override
    public void sortAdapter() {
        Collections.sort(dataCores, DataCore.getDateComparator());
    }


    @Override
    public void setDeletionVisible(boolean deletionVisible) {
        super.setDeletionVisible(deletionVisible);
        for (DataCore dc : dataCores)
            dc.setSelected(false);
        sharedViewModel.notifyLogs();
    }


    public void delete() {
        ArrayList<DataCore> todelete = new ArrayList<>();
        for (DataCore log : dataCores) {
            if (log.isSelected())
                todelete.add(log);
        }
        if (!todelete.isEmpty()) {
            for (DataCore log : todelete) {
                sharedViewModel.getLogsLive().getValue().remove(log);
                log.delete();
            }
            sharedViewModel.notifyLogs();
        }
    }


    public void addPhoto(String _id) {
        ListView listView = getView().findViewById(R.id.logView);
        DataLog log = (DataLog) listView.getSelectedItem();
        if (log == null) {
            log = new DataLog();
        }
        log.set_id(_id);
        MediumInfo info = log.getMediumInfo();
        log.setDate(info.getDateTaken());
        log.save();
        sharedViewModel.getLogsLive().getValue().add(log);
        sharedViewModel.notifyLogs();
        sortAdapter();
    }


    public void save() {
//        for (DataLog n : getLogs())
//            if (n.isModified())
//                n.save();
    }


    protected void getLogsView(DataLog log, DoggyFragment fragment, View view, ViewGroup parent, DataCoreViewHolder holder) {

        holder.tv = view.findViewById(R.id.note_text);
        holder.photo = view.findViewById(R.id.note_photo);
        holder.player = view.findViewById(R.id.note_video);
        holder.display = view.findViewById(R.id.note_display);
        Utils.setTextEndCursor(holder.tv, log.getText());
        Utils.getInstance().setChangeListener(holder.tv, log, "setText");

        this.viewGroup = parent;

        LinearLayout layout = view.findViewById(R.id.note_text_layoàut);
        LinearLayout.LayoutParams param = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.WRAP_CONTENT,
                (!"".equals(log.getPhoto())) ? 1.0f : 0f);
        layout.setLayoutParams(param);

        showMedium(log, holder.photo, true, holder.player, getClickShareMedium(log), getLongClickShare(log));

        TextView tv = view.findViewById(R.id.tv_fname);
        tv.setText(Utils.isFileName ? log.getFileName() : "");

        holder.tvTime.setVisibility(View.VISIBLE);

    }

    protected void getPogressView(DataProgress progress, DoggyFragment fragment, View view, ViewGroup parent, DataCoreViewHolder holder) {

        holder.txorder = view.findViewById(R.id.learning_order);
        holder.txorder.setText(progress.getLearning().getOrder());
        holder.txorder.setTypeface(holder.txorder.getTypeface(), progress.getLearning().isExisting() ? Typeface.BOLD_ITALIC : Typeface.BOLD);

        holder.txdesc = view.findViewById(R.id.learning_description);
        holder.txdesc.setText(progress.getLearning().getDescription());

        holder.ratingBar = view.findViewById(R.id.learning_rating);
        holder.ratingBar.setRating(progress.getRating());

        holder.tvTime.setVisibility(View.INVISIBLE);

        TextView tv = view.findViewById(R.id.progress_rating);
        tv.setText((progress.getProgress() > 0 ? "+" : "") + String.format("%.1f", progress.getProgress()));
    }

    @Override
    public void onResume() {
        super.onResume();
        if (viewGroup == null)
            return;
        for (int i = 0; i < adapter.getCount(); i++) {
            View holder = adapter.getView(i, getView(), viewGroup);
            if (holder != null
                    && holder.getTag() != null
                    && holder.getTag() instanceof DataCoreViewHolder
                    && ((DataCoreViewHolder) holder.getTag()).player != null)
                ((DataCoreViewHolder) holder.getTag()).player.start();
        }
    }

    @Override
    public void onPause() {
        super.onPause();

        if (viewGroup == null)
            return;
        for (int i = 0; i < adapter.getCount(); i++) {
            View holder = adapter.getView(i, getView(), viewGroup);
            if (holder != null
                    && holder.getTag() != null
                    && holder.getTag() instanceof DataCoreViewHolder
                    && ((DataCoreViewHolder) holder.getTag()).player != null
                    && ((DataCoreViewHolder) holder.getTag()).player.isPlaying())
                ((DataCoreViewHolder) holder.getTag()).player.stopPlayback();
        }
    }

    // https://guides.codepath.com/android/Using-an-ArrayAdapter-with-ListView
    public class LogAdapter extends ArrayAdapter<DataCore> {
        private final DoggyFragment fragment;

        public LogAdapter(Context context, DoggyFragment fragment, ArrayList<DataCore> lgs) {
            super(context, 0, lgs);
            this.fragment = fragment;
        }


        @Override
        public View getView(int position, View view, ViewGroup parent) {
            DataCore dataCore = getItem(position);   // normalized comment file name

            DataCoreViewHolder holder = null;

            if (view == null
                    || view.getTag() == null
                    || ((DataCoreViewHolder) view.getTag()).instof != dataCore.getClass()) {

                holder = new DataCoreViewHolder();

                if (dataCore instanceof DataProgress)
                    view = LayoutInflater.from(getContext()).inflate(R.layout.item_note_progress, parent, false);
                else
                    view = LayoutInflater.from(getContext()).inflate(R.layout.item_notes_note, parent, false);

                holder.instof = dataCore.getClass();

                View tb = LayoutInflater.from(getContext()).inflate(R.layout.note_tb, parent, false);
                DoggyFragment.replaceView(view, R.id.tb_in_notes_note, tb);

                holder.tvDate = view.findViewById(R.id.edit_tb_date);
                holder.tvTime = view.findViewById(R.id.edit_tb_time);
                holder.chk = view.findViewById(R.id.chk_log_delete);

                view.setTag(holder);

            } else {
                holder = (DataCoreViewHolder) view.getTag();
            }

            Utils.getInstance().manageDatePicker(fragment, dataCore, holder.tvDate);
            Utils.getInstance().manageTimePicker(fragment, dataCore, holder.tvTime);

            LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) holder.chk.getLayoutParams();
            if (isDeletionVisible()) {
                float width = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 30, getResources().getDisplayMetrics());
                params.width = (int) width;
            } else {
                params.width = 0;
                dataCore.setSelected(false);
            }
            holder.chk.setSelected(dataCore.isSelected());
            holder.chk.setLayoutParams(params);
            Utils.getInstance().manageDeleteCheckBox(dataCore, (CheckBox) holder.chk);

            if (dataCore instanceof DataProgress)
                getPogressView((DataProgress) dataCore, fragment, view, parent, holder);
            else
                getLogsView((DataLog) dataCore, fragment, view, parent, holder);

            return view;
        }
    }

}