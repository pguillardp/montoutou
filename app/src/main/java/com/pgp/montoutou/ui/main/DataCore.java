package com.pgp.montoutou.ui.main;

import android.net.Uri;
import com.pgp.montoutou.MainActivity;
import com.pgp.montoutou.MediumInfo;
import com.pgp.montoutou.Utils;
import com.pgp.montoutou.ui.main.ident.DataIdent;
import com.pgp.montoutou.ui.main.learnings.DataLearning;
import com.pgp.montoutou.ui.main.learnings.DataProgress;
import com.pgp.montoutou.ui.main.logs.DataLog;
import com.pgp.montoutou.ui.main.settings.DataSettings;
import org.json.JSONObject;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Comparator;
import java.util.Date;

public class DataCore {

    public static final String FILE_DATE_PATTERN = "yyyy_MM_dd_HH_mm_ss_SSS";

    protected long ID = -1;           // internal id
    protected String _id = "";        // as stored in media provider
    protected String photoName = "";
    protected String type = "";
    protected Date date = new Date();
    protected boolean modified;
    protected String fileName = "";
    protected boolean selected;
    protected MediumInfo mediumInfo = null;

    public DataCore() {
        date = new Date();
    }

    public long getID() {
        return ID;
    }

    public void setID(long ID) {
        this.ID = ID;
    }

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }


    public static DataCore fromFile(String fileName) {
        DataCore dataCore;
        if (fileName.contains(Utils.IDENT))
            dataCore = new DataIdent(fileName);
        else if (fileName.contains(Utils.LEARNINGS))
            dataCore = new DataLearning(fileName);
        else if (fileName.contains(Utils.SETTINGS))
            dataCore = new DataSettings(fileName);
        else if (fileName.contains(Utils.PROGRESS))
            dataCore = new DataProgress(fileName);
        else
            dataCore = new DataLog(fileName);
        return dataCore;
    }


    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getDogName() {
        return Utils.getInstance().getDogName();
    }

    public boolean isModified() {
        return modified;
    }

    public void setModified(boolean modified) {
        this.modified = modified;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getPhotoName() {
        return photoName;
    }

    public void setPhotoName(String photoname) {
        this.photoName = photoname;
    }

    public Uri getPhoto() {
        getMediumInfo();
        if (mediumInfo != null)
            return mediumInfo.getUri();
        return Uri.parse("");
    }

    public MediumInfo getMediumInfo(boolean force) {
        if (force)
            mediumInfo = null;
        return getMediumInfo();
    }

    public MediumInfo getMediumInfo() {

        if (mediumInfo == null) {
            mediumInfo = Utils.getInstance().getMediumInfo(MainActivity.getInstance(), _id);
        }
        return mediumInfo;
    }


    public boolean hasData() {
        return _id != null && !"".equals(_id);
    }

    public boolean isDataAccessible(String id) {
        getMediumInfo();
        File f = new File(mediumInfo.getData());
        return f.exists();
    }

    public static Comparator<DataCore> getDateComparator() {
        Comparator<DataCore> comparator = new Comparator<DataCore>() {
            @Override
            public int compare(DataCore lhs, DataCore rhs) {
                return rhs.getDate().compareTo(lhs.getDate());   //or whatever your sorting algorithm
            }
        };
        return comparator;
    }

    protected String createFilêname() {
        fileName = dateToString(new Date()) + "_" + type + ".json";
        return fileName;
    }

    public static String dateToString(Date date) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(FILE_DATE_PATTERN);
        return simpleDateFormat.format(date);
    }

    public static Date stringToDate(String sdate) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(FILE_DATE_PATTERN);
        Date date = new Date();
        if ("".equals(sdate))
            return date;
        try {
            sdate = sdate.substring(0, FILE_DATE_PATTERN.length() - 1);
            date = simpleDateFormat.parse(sdate);
            return date;
        } catch (Exception e) {
        }
        return date;
    }

    public static Date changeDateDay(Date date, int year, int monthOfYear, int dayOfMonth) {
        String s = dateToString(date);
        String[] fields = s.split("_");
        fields[0] = String.format("%4d", year);
        fields[1] = String.format("%2d", monthOfYear);
        fields[2] = String.format("%2d", dayOfMonth);
        String t = fields[0];
        for (int i = 1; i < fields.length; i++)
            t += "_" + fields[i];
        Date newdate = stringToDate(t);
        return newdate;
    }


    public void setDateDay(int year, int monthOfYear, int dayOfMonth) {
        date = changeDateDay(date, year, monthOfYear, dayOfMonth);
    }


    public void setDateTime(int hourOfDay, int minute) {
        String s = dateToString(date);
        String[] fields = s.split("_");
        fields[3] = String.format("%2d", hourOfDay);
        fields[4] = String.format("%2d", minute);
        String t = fields[0];
        for (int i = 1; i < fields.length; i++)
            t += "_" + fields[i];
        date = stringToDate(t);
    }


    protected void dateToJson(Date date, JSONObject json, String key) {
        try {
            json.put(key, dateToString(date));
        } catch (Exception e) {
        }
    }

    protected Date jsonToDate(JSONObject json, String key) {
        return stringToDate(json.optString(key, ""));
    }


    public void toJson(JSONObject json) {
        dateToJson(date, json, "date");
        try {
            json.put("_id", _id);
            json.put("photoName", photoName);
        } catch (Exception e) {
        }
    }

    public void fromJson(JSONObject json) {
        _id = json.optString("_id", "");
        date = jsonToDate(json, "date");
        photoName = json.optString("photoName", "");
    }

    public String getJson() {
        JSONObject json = new JSONObject();
        toJson(json);
        return json.toString();
    }


    public void save() {
        JSONObject json = new JSONObject();
        toJson(json);
        Utils.getInstance().saveJson(fileName, type, json);
        modified = false;
    }


    public void read() {
        JSONObject json = Utils.getInstance().jsonFileContent(fileName);
        fromJson(json);

        // backward comliance
        File f = new File(getAbsolutePath());
        if (f.exists()) {
            Date date = new Date(f.lastModified());
            if ((date.getYear() + 1900) < 2010) {
                f.setLastModified(date.getTime());
            }
        }
    }

    public void delete() {
        Utils.getInstance().deleteFile(fileName);
    }

    public void setSelected(boolean val) {
        selected = val;
    }

    public boolean isSelected() {
        return selected;
    }

    public void fromJson(String json) {
        try {
            JSONObject obj = new JSONObject(json);
            fromJson(obj);
        } catch (Exception e) {
        }
    }

    public void setFileModifiedDate(long time) {
        File f = new File(getAbsolutePath());
        f.setLastModified(time);
    }

    public String getAbsolutePath() {
        return Utils.APP_FOLDER + "/" + Utils.getInstance().getDogName() + "/" + fileName;
    }

}
