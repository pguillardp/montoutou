package com.pgp.montoutou.ui.main;

import android.content.Context;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import com.pgp.montoutou.R;
import com.pgp.montoutou.Utils;
import com.pgp.montoutou.ui.main.album.AlbumFragment;
import com.pgp.montoutou.ui.main.ident.IdentFragment;
import com.pgp.montoutou.ui.main.learnings.LearningsFragment;
import com.pgp.montoutou.ui.main.logs.LogsFragment;
import com.pgp.montoutou.ui.main.settings.SettingsFragment;
import com.pgp.montoutou.ui.main.shop.ShopFragment;

/**
 * A [FragmentPagerAdapter] that returns a fragment corresponding to
 * one of the sections/tabs/pages.
 */
public class SectionsPagerAdapter extends FragmentPagerAdapter {

    //@StringRes
    // private static final int[] TAB_TITLES = new int[]{R.string.tab_text_1, R.string.tab_text_2, R.string.tab_text_2, R.string.tab_text_2};
    private final Context mContext;
    private static final int[] TAB_TITLES = new int[]{
            R.string.tab_text_ident,
            R.string.tab_text_album,
            R.string.tab_text_notes,
            R.string.tab_text_learnings,
            //R.string.tab_text_shop,
            R.string.tab_text_settings
    };


    public SectionsPagerAdapter(Context context, FragmentManager fm) {
        super(fm);
        mContext = context;
    }

    @Override
    public Fragment getItem(int position) {
        Fragment f;
        switch (TAB_TITLES[position]) {
            case R.string.tab_text_ident:
                f = IdentFragment.newInstance(position + 1);
                break;
            case R.string.tab_text_album:
                f = AlbumFragment.newInstance(position + 1);
                break;
            case R.string.tab_text_notes:
                f = LogsFragment.newInstance(position + 1);
                break;
            case R.string.tab_text_learnings:
                f = LearningsFragment.newInstance(position + 1);
                break;
            case R.string.tab_text_shop:
                f = ShopFragment.newInstance(position + 1);
                break;
            case R.string.tab_text_settings:
                f = SettingsFragment.newInstance(position + 1);
                break;
            default:
                return null;
        }
        return f;
    }

    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {
        if (position == 0) {
            return Utils.getInstance().getDogName().toUpperCase();
        }
        return mContext.getResources().getString(TAB_TITLES[position]);
    }

    public static int getTabId(int pos) {
        return TAB_TITLES[pos];
    }

    @Override
    public int getCount() {
        return TAB_TITLES.length;
    }
}