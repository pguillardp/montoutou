package com.pgp.montoutou.ui.main.ident;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.*;
import androidx.annotation.NonNull;
import androidx.lifecycle.ViewModelProvider;
import com.pgp.montoutou.MainActivity;
import com.pgp.montoutou.MediumInfo;
import com.pgp.montoutou.R;
import com.pgp.montoutou.Utils;
import com.pgp.montoutou.databinding.FragmentMainBinding;
import com.pgp.montoutou.ui.main.DoggyFragment;
import com.pgp.montoutou.ui.main.SharedViewModel;

import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

/**
 * A placeholder fragment containing a simple view.
 */
public class IdentFragment extends DoggyFragment {

    private static final String ARG_SECTION_NUMBER = "section_number";
    private SharedViewModel sharedViewModel;
    private FragmentMainBinding binding;
    private DataIdent dataident;

    public static IdentFragment newInstance(int index) {
        IdentFragment fragment = new IdentFragment();
        Bundle bundle = new Bundle();
        bundle.putInt(ARG_SECTION_NUMBER, index);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        sharedViewModel = new ViewModelProvider(MainActivity.getInstance()).get(SharedViewModel.class);
        int index = 1;
        if (getArguments() != null) {
            index = getArguments().getInt(ARG_SECTION_NUMBER);
        }
        sharedViewModel.setIndex(index);
        sharedViewModel.getIdentMutableLiveData().observe(this, ident -> {
            if (ident == null)
                return;
            dataident = ident;
            initializeIdent(getView());
        });
        dataident = sharedViewModel.getIdent();
        sharedViewModel.readIdent();
    }

    @Override
    public View onCreateView(
            @NonNull LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
        View root1 = inflater.inflate(R.layout.fragment_ident_page, container, false);
        initializeIdent(root1);
        return root1;
    }

    @Override
    public void onAdd() {
        Utils.getInstance().createDogDialog(getActivity(), false);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
    }

    @Override
    public void selectDog(String dogName) {
        if (sharedViewModel == null)
            return;
        sharedViewModel.readIdent();
        initializeIdent(getView());
    }


    protected void initializeIdent(View ident) {
        TextView tv = ident.findViewById(R.id.txt_dogname);
        tv.setText(Utils.getInstance().getDogName());

        TextView te = ident.findViewById(R.id.txt_birthdate);
        Utils.getInstance().manageDatePicker(this, dataident, te, "birthDate");

        ((TextView) ident.findViewById(R.id.txt_age)).setText(Utils.getInstance().getAge(dataident.getBirthDate()));

        ImageView photoVw = ident.findViewById(R.id.dog_photo);
        showMedium(dataident, photoVw, false, ident.findViewById(R.id.video_view),
                new View.OnClickListener() {
                    public void onClick(View view) {
                        MainActivity.getInstance().insertPhoto();
                    }
                }, new View.OnLongClickListener() {
                    @Override
                    public boolean onLongClick(View view) {
                        return false;
                    }
                });


        photoVw.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                MainActivity.getInstance().insertPhoto();
            }
        });
        
        photoVw.setClipToOutline(true);

        setTodayEvent(ident);

        Button btnSelDog = ident.findViewById(R.id.btn_seldog);
        btnSelDog.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                selectAnotherDoggy();
            }
        });
        btnSelDog.requestFocus();

        EditText tvorder = ident.findViewById(R.id.edit_dogid);
        Utils.setTextEndCursor(tvorder, dataident.getDogId());
        Utils.getInstance().setChangeListener(tvorder, dataident, "setDogId");

        EditText tvrace = ident.findViewById(R.id.edit_race);
        Utils.setTextEndCursor(tvrace, dataident.getRace());
        Utils.getInstance().setChangeListener(tvrace, dataident, "setRace");

        EditText tvrobe = ident.findViewById(R.id.edit_robe);
        Utils.setTextEndCursor(tvrobe, dataident.getRobe());
        Utils.getInstance().setChangeListener(tvrobe, dataident, "setRobe");

        EditText tvlof = ident.findViewById(R.id.edit_lof);
        Utils.setTextEndCursor(tvlof, dataident.getLof());
        Utils.getInstance().setChangeListener(tvlof, dataident, "setLof");
    }


    private void selectAnotherDoggy() {
        final Dialog dialog = new Dialog(getActivity());
        dialog.setContentView(R.layout.select_dog_alert);
        ListView doglist = dialog.findViewById(R.id.seldog_list);
        ArrayAdapter<String> adapter;
        String[] seldog = new String[1];
        seldog[0] = "";
        ArrayList<String> dogFolders = Utils.getInstance().getDogFolders();
        if (dogFolders.size() == 1)
            return;
        dogFolders.remove(Utils.getInstance().getDogName());
        adapter = new ArrayAdapter<String>(getActivity(), R.layout.simple_row, R.id.rowTextView, dogFolders);
        doglist.setAdapter(adapter);
        doglist.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                dialog.dismiss();
                MainActivity.getInstance().selectDog(adapter.getItem(i));
            }
        });
        dialog.findViewById(R.id.btn_seldog_cancel).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.show();
    }

    private void setTodayEvent(View ident) {
        ImageView iv = ident.findViewById(R.id.img_today_event);

        Date currentDate = new Date();

        // convert date to calendar
        iv.setVisibility(View.INVISIBLE);
        Calendar c = Calendar.getInstance();
        c.setTime(currentDate);
        Calendar cb = Calendar.getInstance();
        if (dataident.getBirthDate() != null)
            cb.setTime(dataident.getBirthDate());
        int dom = c.get(Calendar.DAY_OF_MONTH);

        if ((dom == 23 || dom == 24)
                && c.get(Calendar.MONTH) == 1) {
            iv.setImageResource(R.drawable.christmas);
            iv.setVisibility(View.VISIBLE);

        } else if (dataident != null
                && dom == cb.get(Calendar.DAY_OF_MONTH)
                && c.get(Calendar.YEAR) > cb.get(Calendar.YEAR)) {
            iv.setImageResource(R.drawable.bonegift);
            iv.setVisibility(View.VISIBLE);
        }
    }


    public void delete() {
        MainActivity activity = (MainActivity) getActivity();
        AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        builder.setTitle("Supprimer les données de '" + Utils.getInstance().getDogName() + "'?");
        builder.setMessage("Sur ?");
        builder.setPositiveButton("Oui", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        builder.setNegativeButton("Non", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                // Do nothing
                dialog.dismiss();
            }
        });
        AlertDialog alert = builder.create();
        alert.show();
    }

    public void save() {
        if (dataident.isModified())
            dataident.save();
    }

    public void addPhoto(String _id) {
        dataident.set_id(_id);
        dataident.save();
        initializeIdent(getView());
    }

}