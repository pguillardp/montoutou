package com.pgp.montoutou.ui.main.logs;

import com.pgp.montoutou.Utils;
import com.pgp.montoutou.ui.main.DataCore;
import org.json.JSONObject;

public class DataLog extends DataCore {
    protected String text = "";

    public DataLog() {
        type = Utils.LOG;
        createFilêname();
    }

    public DataLog(String fname) {
        type = Utils.LOG;
        fileName = fname;
        read();
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public void toJson(JSONObject json) {
        super.toJson(json);
        try {
            json.put("text", text);
        } catch (Exception e) {
        }
    }

    public void fromJson(JSONObject json) {
        super.fromJson(json);
        text = json.optString("text", "");
    }

    public static DataLog fromFile(String fname) {
        DataLog log = new DataLog();
        log.setFileName(fname);
        log.read();
        return log;
    }

}
