package com.pgp.montoutou.ui.main;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.media.MediaPlayer;
import android.net.Uri;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.VideoView;
import androidx.fragment.app.Fragment;
import com.pgp.montoutou.MainActivity;
import com.pgp.montoutou.MediumInfo;
import com.pgp.montoutou.Utils;
import com.pgp.montoutou.ui.main.album.SharePhotoActivity;
import com.pgp.montoutou.ui.main.ident.IdentFragment;
import com.pgp.montoutou.ui.main.logs.DataLog;
import org.jetbrains.annotations.NotNull;

import java.io.OutputStream;


// TODO: sort adapter after insertion : partial log fra gment
// TOdO: shared fragment data list
// TODO: set date like photo : partial album
// TODO: log/album mother class
// TODO: add photo zoom image + share : partial album
// TODO: select learnings in list
// TODO: start app with dog name query
// TODO: listview recycle

public class DoggyFragment extends Fragment {
    protected boolean deletionVisible;

    // interface
    public void onAdd() {
    }

    public void addPhoto(String _id) {
    }

    public void save() {
    }

    public void delete() {
    }

    public void selectDog(String dogName) {
    }

    public boolean isDeletionVisible() {
        return deletionVisible;
    }

    public void setDeletionVisible(boolean deletionVisible) {
        this.deletionVisible = deletionVisible;
    }

    public void sortAdapter() {
    }

    public void killFocus() {
        View f = this.getActivity().getCurrentFocus();
        if (f != null)
            f.clearFocus();
    }

    protected void setImage(Uri photo, Bitmap photobmp, ImageView iv, int rotate) {
        if ((photo == null || "".equals(photo.toString())) && photobmp == null) {
            iv.setImageResource(0);
            return;
        }
        try {
            if (photobmp != null) {
                iv.setImageBitmap(photobmp);
            } else {
                iv.setRotation(rotate);
                iv.setImageURI(photo);
            }

        } catch (Exception e) {
            iv.setImageResource(0);

        } finally {

        }
    }


    public static void replaceView(View owner, int oldid, View vw) {
        View prev = owner.findViewById(oldid);
        ViewGroup parent = (ViewGroup) prev.getParent();
        final int index = parent.indexOfChild(prev);
        parent.removeView(prev);
        parent.addView(vw, index);
    }

    public void shareMedium(DataLog log) {
        Intent intent = new Intent(this.getActivity(), SharePhotoActivity.class);
        intent.putExtra("current", log.getFileName());
        startActivity(intent);
    }


    public void select() {
    }


    protected void showMedium(DataCore log, ImageView photo, boolean isThumbnail, VideoView player, View.OnClickListener clickShareMedium, View.OnLongClickListener longClickShare) {
        MediumInfo mediumInfo = log.getMediumInfo(true);

        if (player != null
                && player.isPlaying())
            player.stopPlayback();

        if (mediumInfo.isVideo()) {
            player.setVisibility(View.VISIBLE);
            photo.setVisibility(View.INVISIBLE);
            player.setVideoPath(mediumInfo.getData());
            player.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                @Override
                public void onPrepared(MediaPlayer mp) {
                    mp.setLooping(true);
                    mp.setVolume(0, 0);
                    mp.start();
                }
            });
            player.setOnClickListener(clickShareMedium);
            player.setOnLongClickListener(longClickShare);

        } else {

            player.setVisibility(View.INVISIBLE);
            player.stopPlayback();
            photo.setVisibility(View.VISIBLE);

            if (isThumbnail) {
                setImage(mediumInfo.getThumbnail(), mediumInfo.getThumbBitmap(), photo, mediumInfo.getOrientation());

            } else {

                OutputStream fos;
                Bitmap bitmap = BitmapFactory.decodeFile(mediumInfo.getData());

                ExifInterface exif = null;
                try {
                    exif = new ExifInterface(mediumInfo.getData());

                    int orientation = Utils.exifToDegrees(exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, 1));

                    Matrix matrix = new Matrix();
                    matrix.postRotate(orientation);

                    bitmap = Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), matrix, true);
                    photo.setImageBitmap(bitmap);

                } catch (Exception e) {
                } finally {
                }

            }

            photo.setOnClickListener(clickShareMedium);
            photo.setOnLongClickListener(longClickShare);
        }
    }

    @NotNull
    protected View.OnClickListener getClickShareMedium(DataCore log) {
        return new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                shareMedium((DataLog) log);
            }
        };
    }

    @NotNull
    protected View.OnLongClickListener getLongClickShare(DataCore log) {
        return new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                MainActivity.getInstance().shareMedium((DataLog) log);
                return false;
            }
        };
    }

}