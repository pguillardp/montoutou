package com.pgp.montoutou.ui.main.ident;

import com.pgp.montoutou.Utils;
import com.pgp.montoutou.ui.main.DataCore;
import org.json.JSONObject;

import java.util.Date;

public class DataIdent extends DataCore {
    protected String address = "";
    protected Date birthDate = new Date();
    protected String dogId = "";
    protected String race = "";
    protected String robe = "";
    protected String sex = "";
    protected boolean initIdent = false;
    protected String lof = "";

    public DataIdent() {
        type = Utils.IDENT;
        createFilêname();
    }


    public DataIdent(String fname) {
        type = Utils.IDENT;
        fileName = fname;
        read();
    }


    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Date getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(Date birthDate) {
        this.birthDate = birthDate;
    }

    public String getRace() {
        return race;
    }

    public void setRace(String race) {
        this.race = race;
    }

    public String getRobe() {
        return robe;
    }

    public void setRobe(String robe) {
        this.robe = robe;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public boolean isInitIdent() {
        return initIdent;
    }

    public void setInitIdent(boolean initIdent) {
        this.initIdent = initIdent;
    }

    public String getLof() {
        return lof;
    }

    public void setLof(String lof) {
        this.lof = lof;
    }

    @Override
    public void save() {
        if (Utils.getInstance().getDogFolser().exists())
            super.save();
    }

    public void toJson(JSONObject json) {
        super.toJson(json);
        dateToJson(birthDate, json, "birth");
        try {
            json.put("address", address);
            json.put("dogid", dogId);
            json.put("race", race);
            json.put("robe", robe);
            json.put("sex", sex);
            json.put("lof", lof);

        } catch (Exception e) {
        }
    }

    public void fromJson(JSONObject json) {
        super.fromJson(json);
        birthDate = jsonToDate(json, "birth");
        address = json.optString("address", "");
        dogId = json.optString("dogid");
        sex = json.optString("sex");
        robe = json.optString("robe");
        race = json.optString("race");
        lof = json.optString("lof");
    }

    public void setDogId(String dogId) {
        this.dogId = dogId;
    }

    public String getDogId() {
        return dogId;
    }
}
