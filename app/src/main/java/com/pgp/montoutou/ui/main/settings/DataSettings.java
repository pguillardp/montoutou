package com.pgp.montoutou.ui.main.settings;

import com.pgp.montoutou.Utils;
import com.pgp.montoutou.ui.main.DataCore;
import org.json.JSONObject;

public class DataSettings extends DataCore {
    protected String repoUrl = "";
    protected String login = "";
    protected String password = "";
    protected boolean albumDate;

    public DataSettings() {
        type = Utils.SETTINGS;
        createFilêname();
    }

    public DataSettings(String fname) {
        type = Utils.SETTINGS;
        fileName = fname;
        read();
    }

    public String getRepoUrl() {
        return repoUrl;
    }

    public void setRepoUrl(String repoUrl) {
        this.repoUrl = repoUrl;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public boolean isAlbumDate() {
        return albumDate;
    }

    public void setAlbumDate(boolean albumDate) {
        this.albumDate = albumDate;
    }

    public void toJson(JSONObject json) {
        super.toJson(json);
        try {
            json.put("repoUrl", repoUrl);
            json.put("login", login);
            json.put("password", password);
            json.put("albumdate", albumDate);
        } catch (Exception e) {
        }
    }

    public void fromJson(JSONObject json) {
        super.fromJson(json);
        try {
            repoUrl = json.optString("repoUrl", "");
            login = json.optString("login", "");
            password = json.optString("password", "");
            albumDate = json.getBoolean("albumdate");
        } catch (Exception e) {
        }
    }
}
