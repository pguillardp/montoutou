package com.pgp.montoutou.ui.main.album;

import android.content.Context;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.*;
import androidx.annotation.NonNull;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.pgp.montoutou.*;
import com.pgp.montoutou.databinding.FragmentMainBinding;
import com.pgp.montoutou.ui.main.DataCore;
import com.pgp.montoutou.ui.main.DoggyFragment;
import com.pgp.montoutou.ui.main.SharedViewModel;
import com.pgp.montoutou.ui.main.logs.DataLog;


import java.util.ArrayList;
import java.util.Collections;

/**
 * A placeholder fragment containing a simple view.
 */
public class AlbumFragment extends DoggyFragment {

    private final static int SPAN = 4;
    private static final String ARG_SECTION_NUMBER = "section_number";
    private SharedViewModel sharedViewModel;
    private FragmentMainBinding binding;
    private PhotoAdapter adapter;
    private ArrayList<DataLog> photos;
    private final int selectedNote = -1;
    private boolean isZoom = false;
    private RecyclerView recyclerView;


    public static AlbumFragment newInstance(int index) {
        AlbumFragment fragment = new AlbumFragment();
        Bundle bundle = new Bundle();
        bundle.putInt(ARG_SECTION_NUMBER, index);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        sharedViewModel = new ViewModelProvider(MainActivity.getInstance()).get(SharedViewModel.class);
        int index = 1;
        if (getArguments() != null) {
            index = getArguments().getInt(ARG_SECTION_NUMBER);
        }
        sharedViewModel.setIndex(index);
        photos = new ArrayList<>();
        sharedViewModel.readLogs();
        sharedViewModel.getLogsLive().observe(this, logs -> {
            if (logs != null)
                filterSort();
        });
        filterSort();
    }

    @Override
    public View onCreateView(
            @NonNull LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {

        binding = FragmentMainBinding.inflate(inflater, container, false);

        View view = inflater.inflate(R.layout.fragment_album_page, container, false);
        adapter = new PhotoAdapter(getPhotos(), this.getContext());
        this.recyclerView = view.findViewById(R.id.album_recycler);
        recyclerView.setLayoutManager(new GridLayoutManager(getContext(), SPAN, RecyclerView.VERTICAL, false));
        recyclerView.setAdapter(adapter);

        setDeletionVisible(false);

        return view;
    }


    public ArrayList<DataLog> getPhotos() {
        return photos;
    }

    @Override
    public void onResume() {
        super.onResume();
        for (int i = 0; i < adapter.getItemCount(); i++) {
            PhotoAdapter.ViewHolder holder = (PhotoAdapter.ViewHolder) recyclerView.findViewHolderForAdapterPosition(i);
            if (holder != null)
                holder.player.start();
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        for (int i = 0; i < adapter.getItemCount(); i++) {
            PhotoAdapter.ViewHolder holder = (PhotoAdapter.ViewHolder) recyclerView.findViewHolderForAdapterPosition(i);
            if (holder != null)
                holder.player.stopPlayback();
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
    }

    public void filterSort() {
        photos.clear();
        if (adapter != null)
            adapter.notifyDataSetChanged();
        for (DataLog d : sharedViewModel.getLogsLive().getValue())
            if (d.getPhoto() != null && !"".equals(d.getPhoto().toString()))
                photos.add(d);
        sortAdapter();
        if (adapter != null)
            adapter.notifyDataSetChanged();
    }


    public void onAdd() {
        MainActivity.getInstance().insertPhoto();
    }

    @Override
    public void sortAdapter() {
        Collections.sort(photos, DataCore.getDateComparator());
    }


    public void addPhoto(String _id) {
        DataLog log = new DataLog();
        log.set_id(_id);
        MediumInfo info = log.getMediumInfo();
        log.setDate(info.getDateTaken());
        log.save();
        sharedViewModel.getLogsLive().getValue().add(log);
        sharedViewModel.notifyLogs();
    }


    @Override
    public void setDeletionVisible(boolean deletionVisible) {
        super.setDeletionVisible(deletionVisible);
        for (int i = 0; i < getPhotos().size(); i++)
            getPhotos().get(i).setSelected(false);
        adapter.notifyDataSetChanged();
    }


    public void delete() {
        ArrayList<DataCore> todelete = new ArrayList<>();
        DataCore log;
        for (int i = 0; i < getPhotos().size(); i++) {
            log = getPhotos().get(i);
            if (log.isSelected()) {
                todelete.add(log);
                log.setSelected(false);
            }
        }
        if (!todelete.isEmpty()) {
            for (DataCore dc : todelete) {
                sharedViewModel.getLogsLive().getValue().remove(dc);
                dc.delete();
                sharedViewModel.notifyLogs();
            }
        }
    }

    public boolean isZoom() {
        return isZoom;
    }

    public void setZoom(boolean zoom) {
        isZoom = zoom;

        RecyclerView recyclerView = getView().findViewById(R.id.album_recycler);
        GridLayoutManager layoutManager = (GridLayoutManager) recyclerView.getLayoutManager();

        View av = getView();

        if (zoom) {
            layoutManager.setSpanCount(1);
            showWidgets(av, View.VISIBLE);

        } else {
            layoutManager.setSpanCount(SPAN);
            showWidgets(av, View.INVISIBLE);
        }
        filterSort();       // reinit views...
    }

    private void showWidgets(View av, int visible) {
        av.findViewById(R.id.legend_button).setVisibility(visible);
        av.findViewById(R.id.txt_legend).setVisibility(visible);
        av.findViewById(R.id.btn_share_photo).setVisibility(visible);
        av.findViewById(R.id.btn_album).setVisibility(visible);
        av.findViewById(R.id.txt_legend).setEnabled(visible == View.VISIBLE);
    }


    public class PhotoAdapter extends RecyclerView.Adapter<PhotoAdapter.ViewHolder> {
        private final ArrayList<DataLog> locallogs;
        private final Context mcontext;

        public PhotoAdapter(ArrayList<DataLog> locallogs, Context mcontext) {
            this.locallogs = locallogs;
            this.mcontext = mcontext;
        }

        // Create new views (invoked by the layout manager)
        @Override
        public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
            // Create a new view, which defines the UI of the list item
            View view = LayoutInflater.from(viewGroup.getContext())
                    .inflate(R.layout.item_album_photo, viewGroup, false);
            int height = viewGroup.getMeasuredHeight() / 5;
            ViewGroup.LayoutParams p = view.getLayoutParams();
            p.height = p.width;
            view.setLayoutParams(p);
            ViewHolder holder = new ViewHolder(view, viewGroup);

            return holder;
        }

        // Replace the contents of a view (invoked by the layout manager)
        @Override
        public void onBindViewHolder(ViewHolder holder, final int position) {
            DataLog log = locallogs.get(position);

            Utils.getInstance().manageDatePicker(AlbumFragment.this, log, holder.tvDate);
            holder.tvDate.setVisibility(sharedViewModel.getDataSettings().isAlbumDate() ? View.VISIBLE : View.INVISIBLE);

            EditTextV2 ed = AlbumFragment.this.getView().findViewById(R.id.txt_legend);
            Utils.setTextEndCursor(ed, log.getText());
            Utils.getInstance().setChangeListener(ed, log, "setText");

            showMedium(log, holder.photo, true, holder.player, getClickShareMedium(log), getLongClickShare(log));

            // imageview vs mediaplayer
            View av = getView();
            ((com.pgp.montoutou.EditTextV2) av.findViewById(R.id.txt_legend)).setText(log.getText());

            holder.chk.setVisibility(isDeletionVisible() ? View.VISIBLE : View.INVISIBLE);
            holder.chk.setSelected(log.isSelected());
            Utils.getInstance().manageDeleteCheckBox(log, (CheckBox) holder.chk);
        }

        // Return the size of your dataset (invoked by the layout manager)
        @Override
        public int getItemCount() {
            return locallogs.size();
        }


        /**
         * Provide a reference to the type of views that you are using
         * (custom ViewHolder).
         */
        public class ViewHolder extends RecyclerView.ViewHolder {
            private final VideoView player;
            public TextView tvDate;
            public ImageView photo;
            public View chk;
            public boolean isZoom = false;

            public ViewHolder(View view, ViewGroup viewGroup) {
                super(view);
                tvDate = view.findViewById(R.id.edit_tb_date);
                photo = view.findViewById(R.id.note_photo);
                player = view.findViewById(R.id.note_video);
                chk = view.findViewById(R.id.chk_log_delete);

                if (this.isZoom != AlbumFragment.this.isZoom) {
                    this.isZoom = AlbumFragment.this.isZoom;
                }

                float tsize = 10;
                View av = AlbumFragment.this.getView();

                ViewGroup.LayoutParams mainParams = av.getLayoutParams();

                int height = mainParams.height;
                int width = mainParams.width;
                if (isZoom) {
                    LinearLayout.LayoutParams parms = new LinearLayout.LayoutParams(width, height);
                    photo.setLayoutParams(parms);
                    showWidgets(av, View.VISIBLE);

                } else {
                    showWidgets(av, View.INVISIBLE);
                }
                System.out.println("w: " + photo.getLayoutParams().width + " -h: " + photo.getLayoutParams().height);

                tvDate.setTextSize(TypedValue.COMPLEX_UNIT_SP, tsize);
            }


        }
    }
}
