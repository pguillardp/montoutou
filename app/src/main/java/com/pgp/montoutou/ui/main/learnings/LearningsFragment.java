package com.pgp.montoutou.ui.main.learnings;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Typeface;
import android.os.Bundle;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.*;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.lifecycle.ViewModelProvider;
import com.pgp.montoutou.MainActivity;
import com.pgp.montoutou.R;
import com.pgp.montoutou.Utils;
import com.pgp.montoutou.databinding.FragmentMainBinding;
import com.pgp.montoutou.ui.main.DataCore;
import com.pgp.montoutou.ui.main.DoggyFragment;
import com.pgp.montoutou.ui.main.SharedViewModel;
import com.pgp.montoutou.ui.main.logs.DataCoreViewHolder;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.Locale;

/**
 * A placeholder fragment containing a simple view.
 */
public class LearningsFragment extends DoggyFragment {

    private static final String ARG_SECTION_NUMBER = "section_number";
    private SharedViewModel sharedViewModel;
    private FragmentMainBinding binding;
    private final int selectedLearning = -1;
    LearningsAdapter adapter;
    private boolean sort = false;

    public static LearningsFragment newInstance(int index) {
        LearningsFragment fragment = new LearningsFragment();
        Bundle bundle = new Bundle();
        bundle.putInt(ARG_SECTION_NUMBER, index);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        sharedViewModel = new ViewModelProvider(MainActivity.getInstance()).get(SharedViewModel.class);
        int index = 1;
        if (getArguments() != null) {
            index = getArguments().getInt(ARG_SECTION_NUMBER);
        }
        sharedViewModel.setIndex(index);
        //sharedViewModel.readLearnings();
        sharedViewModel.getLearnings().observe(this, learnings -> {
            if (adapter != null)
                adapter.notifyDataSetChanged();
        });
    }

    @Override
    public View onCreateView(
            @NonNull LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {

        binding = FragmentMainBinding.inflate(inflater, container, false);
        View root3 = inflater.inflate(R.layout.fragment_learnings_page, container, false);
        initialize(root3);
        return root3;
    }

    protected void initialize(View learningsView) {
        ListView listView = learningsView.findViewById(R.id.learningView);
        adapter = new LearningsAdapter(getActivity(), this, getLearnings());
        listView.setAdapter(adapter);
        sort();
        refreshStats(learningsView);
    }

    public ArrayList<DataLearning> getLearnings() {
        return sharedViewModel.getLearnings().getValue();
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
    }


    @Override
    public void selectDog(String dogName) {
        sharedViewModel.readLearnings();
        sharedViewModel.notifyLearnings();
    }


    private void refreshStats(View learningsView) {
        TextView txt = learningsView.findViewById(R.id.text_orders);
        ArrayList<DataLearning> learnings = sharedViewModel.getLearnings().getValue();
        txt.setText("Consignes : " + learnings.size());
        float avg = 0;
        for (DataLearning l : learnings) {
            avg += l.getRating();
        }
        if (!learnings.isEmpty())
            avg /= learnings.size() * 5;

        txt = learningsView.findViewById(R.id.text_average);
        txt.setText(String.format("%.1f", avg));
        RatingBar ratingBar = learningsView.findViewById(R.id.average_rating);
        ratingBar.setRating(avg);
    }


    @Override
    public void sortAdapter() {
        sort = true;
    }

    private void sort() {
        Collections.sort(getLearnings(), DataLearning.getOrderComparator());
        adapter.notifyDataSetChanged();
    }

    @Override
    public void select() {
        if (sharedViewModel != null && getLearnings() != null)
            sort();
    }

    @Override
    public void onResume() {
        super.onResume();
        if (sort) {
            sort();
            sort = false;
        }
    }

    @Override
    public void setDeletionVisible(boolean deletionVisible) {
        super.setDeletionVisible(deletionVisible);
        for (DataCore dc : getLearnings())
            dc.setSelected(false);
        adapter.notifyDataSetChanged();
    }

    public void delete() {
        ArrayList<DataCore> todelete = new ArrayList<>();
        for (DataCore learning : getLearnings()) {
            if (learning.isSelected())
                todelete.add(learning);
        }
        if (!todelete.isEmpty()) {
            for (DataCore learning : todelete) {
                getLearnings().remove(learning);
                sharedViewModel.getProgress().removeAll(((DataLearning) learning).getProgressList());
                adapter.remove((DataLearning) learning);
                learning.delete();
            }
            sharedViewModel.notifyLearnings();
            refreshStats(getView());
        }
    }


    public void onAdd() {
        chooseAdditionalLearnings();
        refreshStats(getView());
    }

    public void save() {
    }

    // https://guides.codepath.com/android/Using-an-ArrayAdapter-with-ListView
    private class LearningsAdapter extends ArrayAdapter<DataLearning> {
        private final DoggyFragment fragment;

        public LearningsAdapter(Context context, DoggyFragment fragment, ArrayList<DataLearning> l) {
            super(context, 0, l);
            this.fragment = fragment;
        }

        @Nullable
        @Override
        public DataLearning getItem(int position) {
            return super.getItem(position);
        }

        @Override
        public View getView(int position, View learningview, ViewGroup parent) {
            DataLearning learning = getItem(position);

            DataCoreViewHolder holder = null;

            if (learningview == null) {
                learningview = LayoutInflater.from(getContext()).inflate(R.layout.item_learning, parent, false);
                holder = new DataCoreViewHolder();

                holder.chk = learningview.findViewById(R.id.chk_learning_delete);
                holder.tvorder = learningview.findViewById(R.id.learning_order);
                holder.tvdesc = learningview.findViewById(R.id.learning_description);
                holder.ratingBar = learningview.findViewById(R.id.learning_rating);

                learningview.setTag(holder);

            } else {
                holder = (DataCoreViewHolder) learningview.getTag();
            }

            //holder.tvorder.setText(learning.getOrder());
            Utils.setTextEndCursor(holder.tvorder, learning.getOrder());
            Utils.getInstance().setChangeListener(holder.tvorder, learning, "setOrder", LearningsFragment.this);

            //holder.tvdesc.setText(learning.getDescription());
            Utils.setTextEndCursor(holder.tvdesc, learning.getDescription());
            Utils.getInstance().setChangeListener(holder.tvdesc, learning, "setDescription");

            holder.ratingBar.setRating(learning.getRating());
            holder.ratingBar.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
                @Override
                public void onRatingChanged(RatingBar ratingBar, float v, boolean fromTouch) {
                    if (!fromTouch)
                        return;
                    learning.setRating(v);
                    learning.save();
                    learning.updateProgress(sharedViewModel.getProgressLive().getValue());
                    synchronized (sharedViewModel.getProgressLive()) {
                        ArrayList<DataProgress> value = sharedViewModel.getProgressLive().getValue();
                        sharedViewModel.getProgressLive().setValue(null);
                        sharedViewModel.getProgressLive().setValue(value);
                        sharedViewModel.getProgressLive().notify();
                    }
                    refreshStats(LearningsFragment.this.getView());
                }
            });

            LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) holder.chk.getLayoutParams();
            if (isDeletionVisible()) {
                float width = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 30, getResources().getDisplayMetrics());
                params.width = (int) width;
            } else {
                params.width = 0;
                learning.setSelected(false);
            }
            holder.chk.setLayoutParams(params);
            holder.chk.setSelected(learning.isSelected());
            Utils.getInstance().manageDeleteCheckBox(learning, (CheckBox) holder.chk);

            TextView tv = learningview.findViewById(R.id.tv_fname);
            tv.setText(Utils.isFileName ? learning.getFileName() : "");

            return learningview;
        }
    }


    // https://demonuts.com/android-custom-dialog-with-listview/
    protected void chooseAdditionalLearnings() {
        ArrayList<DataLearning> items = Utils.getInstance().getResourceLearnings();
        for (DataLearning dl : items) {
            for (DataLearning dle : getLearnings()) {
                if (dle.getOrder().toLowerCase(Locale.ROOT).equals(dl.getOrder().toLowerCase(Locale.ROOT))) {
                    dl.setExisting(true);
                }
            }
        }

        final Dialog dialog = new Dialog(getActivity());
        dialog.setCancelable(true);
        dialog.setContentView(R.layout.add_learnings_dialog);

        ListView listView = dialog.findViewById(R.id.listview);
        ArrayAdapter<DataLearning> arrayAdapter = new AddLearningAdapter(getActivity(), items);
        listView.setAdapter(arrayAdapter);

        ((TextView) dialog.findViewById(R.id.txt_education)).setText(getResources().getString(R.string.dlg_learning_help));

        TextView btndialog = dialog.findViewById(R.id.btndialog_ok);
        btndialog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                for (DataLearning dl : items) {
                    if (!dl.isSelected())
                        continue;
                    DataLearning learning = new DataLearning();
                    learning.setOrder(dl.getOrder());
                    learning.setDescription(dl.getDescription());
                    learning.setRating(0, new Date());
                    learning.save();
                    getLearnings().add(learning);

                    // ms filename...
                    try {
                        wait(2);
                    } catch (Exception r) {
                    }
                }
                sort();
            }
        });
        dialog.findViewById(R.id.btndialog_cancel).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.show();
    }


    private class AddLearningAdapter extends ArrayAdapter<DataLearning> {
        public AddLearningAdapter(Context context, ArrayList<DataLearning> items) {
            super(context, 0, items);
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            DataLearning item = getItem(position);
            if (convertView == null) {
                convertView = LayoutInflater.from(getContext()).inflate(R.layout.add_learnings_dialog_item, parent, false);
            }

            TextView textView = convertView.findViewById(R.id.add_learning_order);
            textView.setText(item.order);
            textView.setTypeface(textView.getTypeface(), item.isExisting() ? Typeface.BOLD_ITALIC : Typeface.BOLD);

            ((TextView) convertView.findViewById(R.id.add_learning_description)).setText(item.description);

            ((CheckBox) convertView.findViewById(R.id.add_learning_chk)).setChecked(item.isSelected());
            convertView.findViewById(R.id.add_learning_chk).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    item.setSelected(!item.isSelected());
                }
            });
            return convertView;
        }
    }
}