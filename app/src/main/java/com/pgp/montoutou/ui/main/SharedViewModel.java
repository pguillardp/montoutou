package com.pgp.montoutou.ui.main;

import androidx.arch.core.util.Function;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.Transformations;
import androidx.lifecycle.ViewModel;
import com.pgp.montoutou.Utils;
import com.pgp.montoutou.ui.main.ident.DataIdent;
import com.pgp.montoutou.ui.main.learnings.DataLearning;
import com.pgp.montoutou.ui.main.learnings.DataProgress;
import com.pgp.montoutou.ui.main.logs.DataLog;
import com.pgp.montoutou.ui.main.settings.DataSettings;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public class SharedViewModel extends ViewModel {

    private final HashMap<String, DataCore> dataCores = new HashMap<>();

    private final MutableLiveData<Integer> mIndex = new MutableLiveData<>();

    private final MutableLiveData<ArrayList<DataLog>> logsLive = new MutableLiveData<ArrayList<DataLog>>();
    private final ArrayList<DataLog> logs = new ArrayList<>();

    private final MutableLiveData<ArrayList<DataLearning>> learningsLive = new MutableLiveData<ArrayList<DataLearning>>();
    private final ArrayList<DataLearning> learnings = new ArrayList<>();

    private final MutableLiveData<ArrayList<DataProgress>> progressLive = new MutableLiveData<ArrayList<DataProgress>>();
    private final ArrayList<DataProgress> progress = new ArrayList<>();

    private final MutableLiveData<DataIdent> identMutableLiveData = new MutableLiveData<>();
    private final DataIdent dataident = new DataIdent();

    private final MutableLiveData<DataSettings> settingsMutableLiveData = new MutableLiveData<>();
    private DataSettings dataSettings = null;


    public void readEvents() {
        dataCores.clear();
        for (String fname : Utils.getInstance().getEvtList()) {
            if (!dataCores.containsKey(fname))
                dataCores.put(fname, DataCore.fromFile(fname));
        }
    }


    public void readLogs() {
        readEvents();
        logs.clear();
        for (Map.Entry<String, DataCore> it : dataCores.entrySet()) {
            if (it.getKey().contains(Utils.LOG))
                logs.add((DataLog) it.getValue());
        }

        Collections.sort(logs, DataCore.getDateComparator());
        logsLive.setValue(logs);
    }


    public void readLearnings() {
        readEvents();
        learnings.clear();
        for (Map.Entry<String, DataCore> it : dataCores.entrySet())
            if (it.getKey().contains(Utils.LEARNINGS)) {
                DataLearning learning = (DataLearning) it.getValue();
                learnings.add(learning);
            }
        Collections.sort(learnings, DataLearning.getOrderComparator());
        learningsLive.setValue(learnings);
    }


    public void computeProgress() {
        progress.clear();
        for (DataLearning l : learnings)
            l.updateProgress(progress);
        Collections.sort(progress, DataCore.getDateComparator());
        progressLive.setValue(progress);
    }


    public void readIdent() {
        for (String fname : Utils.getInstance().getEvtList()) {
            if (fname.contains(Utils.IDENT)) {
                JSONObject json = Utils.getInstance().jsonFileContent(fname);
                dataident.fromJson(json);
                dataident.setFileName(fname);
            }
        }

    }


    public void readDataSettings() {
        readEvents();
        for (Map.Entry<String, DataCore> it : dataCores.entrySet())
            if (it.getKey().contains(Utils.SETTINGS)) {
                dataSettings = (DataSettings) it.getValue();
                settingsMutableLiveData.setValue(dataSettings);
                synchronized (settingsMutableLiveData) {
                    settingsMutableLiveData.notify();
                }
                break;
            }
    }


    private final LiveData<String> mText = Transformations.map(mIndex, new Function<Integer, String>() {
        @Override
        public String apply(Integer input) {
            return "Hello world from section: " + input;
        }
    });

    public void setIndex(int index) {
        mIndex.setValue(index);
    }

    public LiveData<String> getText() {
        return mText;
    }

    public void notifyLearnings() {
        learningsLive.setValue(learnings);
        progressLive.setValue(progress);
    }

    public void notifyLogs() {
        logsLive.setValue(null);
        logsLive.setValue(logs);
    }

    public MutableLiveData<ArrayList<DataLog>> getLogsLive() {
        return logsLive;
    }

    public MutableLiveData<ArrayList<DataLearning>> getLearnings() {
        return learningsLive;
    }

    public MutableLiveData<DataSettings> getSettingsMutableLiveData() {
        return settingsMutableLiveData;
    }

    public MutableLiveData<ArrayList<DataLearning>> getLearningsLive() {
        return learningsLive;
    }

    public MutableLiveData<DataIdent> getIdentMutableLiveData() {
        return identMutableLiveData;
    }

    public void saveLearnings() {
        for (DataLearning l : learnings) {
            if (l.isModified())
                l.save();
        }
    }

    public ArrayList<DataProgress> getProgress() {
        return progress;
    }

    public void saveLogs() {
        for (DataLog l : logs) {
            if (l.isModified())
                l.save();
        }
    }

    public DataIdent getIdent() {
        return dataident;
    }

    public DataSettings getDataSettings() {
        if (dataSettings != null)
            return dataSettings;
        dataSettings = new DataSettings();
        readSettings();
        return dataSettings;
    }

    public void readSettings() {
        for (String fname : Utils.getInstance().getEvtList()) {
            if (fname.contains(Utils.SETTINGS)) {
                JSONObject json = Utils.getInstance().jsonFileContent(fname);
                dataSettings.fromJson(json);
                dataSettings.setFileName(fname);
                break;
            }
        }
    }


    public MutableLiveData<ArrayList<DataProgress>> getProgressLive() {
        return progressLive;
    }

    public void updateDataCore(String fileName) {
        if (fileName.contains(Utils.IDENT)) {
            JSONObject json = Utils.getInstance().jsonFileContent(fileName);
            dataident.fromJson(json);
            dataident.setFileName(fileName);
            identMutableLiveData.setValue(null);
            identMutableLiveData.setValue(dataident);

        } else if (fileName.contains(Utils.SETTINGS)) {
            JSONObject json = Utils.getInstance().jsonFileContent(fileName);
            dataSettings.fromJson(json);
            dataSettings.setFileName(fileName);
            settingsMutableLiveData.setValue(null);
            settingsMutableLiveData.setValue(dataSettings);

        } else if (fileName.contains(Utils.LEARNINGS)) {
            readLearnings();
            notifyLearnings();
            notifyProgress();

        } else if (fileName.contains(Utils.LOG)) {
            readLogs();
            notifyLogs();
        }

    }

    public void notifyProgress() {
        progressLive.setValue(null);
        progressLive.setValue(progress);
    }

    public void notifyDeleted(String fileName) {
        if (fileName.contains(Utils.LEARNINGS)) {
            readLearnings();
            notifyLearnings();
            notifyProgress();

        } else if (fileName.contains(Utils.LOG)) {
            readLogs();
            notifyLogs();
        }
    }
}