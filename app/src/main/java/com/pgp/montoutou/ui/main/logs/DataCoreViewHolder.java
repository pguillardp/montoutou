package com.pgp.montoutou.ui.main.logs;

import android.view.View;
import android.widget.*;
import androidx.constraintlayout.widget.ConstraintLayout;
import com.pgp.montoutou.ui.main.DataCore;

public class DataCoreViewHolder {

    public TextView tvDate;
    public ImageView photo;
    public TextView tvTime;
    public EditText tv;
    public View chk;
    public EditText tvorder;
    public TextView txorder;
    public EditText tvdesc;
    public TextView txdesc;
    public RatingBar ratingBar;
    public Class<? extends DataCore> instof;
    public VideoView player;
    public ConstraintLayout display;
}
