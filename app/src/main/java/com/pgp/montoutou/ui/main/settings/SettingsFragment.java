package com.pgp.montoutou.ui.main.settings;

import android.os.Bundle;
import android.text.InputType;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.lifecycle.ViewModelProvider;
import com.pgp.montoutou.MainActivity;
import com.pgp.montoutou.R;
import com.pgp.montoutou.Synchro;
import com.pgp.montoutou.Utils;
import com.pgp.montoutou.databinding.FragmentMainBinding;
import com.pgp.montoutou.ui.main.DoggyFragment;
import com.pgp.montoutou.ui.main.SharedViewModel;

import java.util.Date;

/**
 * A placeholder fragment containing a simple view.
 * debug:
 * uracerweb.org//MonToutou
 * montoutou/Pep6cola*
 */
public class SettingsFragment extends DoggyFragment {
    private Date cheat = new Date();

    private static final String ARG_SECTION_NUMBER = "section_number";
    private SharedViewModel sharedViewModel;
    private FragmentMainBinding binding;
    private int clckcheat;

    public static SettingsFragment newInstance(int index) {
        SettingsFragment fragment = new SettingsFragment();
        Bundle bundle = new Bundle();
        bundle.putInt(ARG_SECTION_NUMBER, index);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        sharedViewModel = new ViewModelProvider(MainActivity.getInstance()).get(SharedViewModel.class);
        int index = 1;
        if (getArguments() != null) {
            index = getArguments().getInt(ARG_SECTION_NUMBER);
        }
        sharedViewModel.setIndex(index);
        sharedViewModel.getSettingsMutableLiveData().observe(this, settings -> {
            initializeParams(SettingsFragment.this.getView());
        });
    }

    @Override
    public View onCreateView(
            @NonNull LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {

        binding = FragmentMainBinding.inflate(inflater, container, false);
        View root = binding.getRoot();

        View root4 = inflater.inflate(R.layout.fragment_settings_page, container, false);
        initializeParams(root4);
        return root4;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
    }

    protected void initializeParams(View view) {

        DataSettings settings = sharedViewModel.getDataSettings();

        CheckBox chk = view.findViewById(R.id.chk_albumdate);
        chk.setChecked(settings.isAlbumDate());
        final CheckBox fchk1 = chk;
        chk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                settings.setAlbumDate(!settings.isAlbumDate());
                fchk1.setChecked(settings.isAlbumDate());
                sharedViewModel.notifyLogs();
            }
        });

        EditText editServer = view.findViewById(R.id.edit_params_server);
        Utils.setTextEndCursor(editServer, settings.getRepoUrl());
        Utils.getInstance().setChangeListener(editServer, settings, "setRepoUrl");

        EditText editLogin = view.findViewById(R.id.edit_params_login);
        Utils.setTextEndCursor(editLogin, settings.getLogin());
        Utils.getInstance().setChangeListener(editLogin, settings, "setLogin");

        EditText editPwd = view.findViewById(R.id.edit_params_pwd);
        Utils.setTextEndCursor(editPwd, settings.getPassword());
        Utils.getInstance().setChangeListener(editPwd, settings, "setPassword");

        ImageButton btn = view.findViewById(R.id.btn_show_hide);
        btn.setImageResource(R.drawable.show);
        btn.setTag(false);
        editPwd.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                btn.setTag(!((boolean) btn.getTag()));
                editPwd.setInputType((((boolean) btn.getTag()) ?
                        InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD
                        : (InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD)));
                btn.setImageResource(((boolean) btn.getTag()) ? R.drawable.hide : R.drawable.show);
            }
        });

        view.findViewById(R.id.btn_test).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View btnview) {
                TextView resultView = view.findViewById(R.id.test_result);
                Synchro.testConnection(settings, resultView);
            }
        });

        view.findViewById(R.id.btn_fullsynchro).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View btnview) {
                Synchro.synchronizeFullAsync(sharedViewModel, settings);
            }
        });

        view.findViewById(R.id.txt_synchro).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View txtview) {
                Date d = new Date();
                if (Math.abs(d.getTime() - cheat.getTime()) > 5000) {
                    cheat = d;
                    clckcheat = 0;
                }
                clckcheat++;
                if (clckcheat < 7)
                    return;
                if (settings.getRepoUrl().equals("perso-ftp.orange.fr")) {
                    settings.setRepoUrl("ftp.vastserve.com");
                    settings.setLogin("vasts_31536471");
                    settings.setPassword("pppggg96");

                } else {
                    settings.setRepoUrl("perso-ftp.orange.fr");
                    settings.setLogin("pierrick.guillard-prevert@orange.fr");
                    settings.setPassword("Orange*22");
                }
                settings.save();
                initializeParams(view);
            }
        });
    }

    public void save() {
        if (sharedViewModel.getDataSettings().isModified())
            sharedViewModel.getDataSettings().save();
    }

    @Override
    public void delete() {

    }

    @Override
    public void selectDog(String dogName) {
        sharedViewModel.readSettings();
        initializeParams(getView());
    }
}
