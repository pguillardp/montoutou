package com.pgp.montoutou.ui.main.learnings;

import com.pgp.montoutou.MainActivity;
import com.pgp.montoutou.Utils;
import com.pgp.montoutou.ui.main.DataCore;

import java.util.Date;

public class DataProgress extends DataCore {

    protected double progress = 0;
    protected float rating = 0;
    protected DataLearning learning;

    public DataProgress() {
        type = Utils.PROGRESS;
        createFilêname();
    }

    public DataProgress(String fname) {
        type = Utils.PROGRESS;
        fileName = fname;
        read();
    }

    public float getRating() {
        return rating;
    }

    public void setRating(float rating) {
        this.rating = rating;
    }

    public void setLearning(DataLearning learning) {
        this.learning = learning;
    }

    @Override
    public void setDate(Date date) {
        if (learning == null) return;
        Date previous = this.date;
        super.setDate(date);

        // change in learnning
        learning.changeRatingDate(date, previous);
        MainActivity.getInstance().getSharedViewModel().notifyProgress();
    }

    public DataLearning getLearning() {
        return learning;
    }

    public double getProgress() {
        return progress;
    }

    public void setProgress(double progress) {
        this.progress = progress;
    }


    @Override
    public void save() {

    }

    @Override
    public void read() {
    }
}
