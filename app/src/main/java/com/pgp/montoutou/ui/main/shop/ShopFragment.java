package com.pgp.montoutou.ui.main.shop;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import androidx.annotation.NonNull;
import androidx.lifecycle.ViewModelProvider;
import com.pgp.montoutou.MainActivity;
import com.pgp.montoutou.R;
import com.pgp.montoutou.databinding.FragmentMainBinding;
import com.pgp.montoutou.ui.main.DoggyFragment;
import com.pgp.montoutou.ui.main.SharedViewModel;

/**
 * A placeholder fragment containing a simple view.
 */
public class ShopFragment extends DoggyFragment {

    private static final String ARG_SECTION_NUMBER = "section_number";
    private SharedViewModel sharedViewModel;
    private FragmentMainBinding binding;
    private WebView webView;

    public static ShopFragment newInstance(int index) {
        ShopFragment fragment = new ShopFragment();
        Bundle bundle = new Bundle();
        bundle.putInt(ARG_SECTION_NUMBER, index);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        sharedViewModel = new ViewModelProvider(MainActivity.getInstance()).get(SharedViewModel.class);
        int index = 1;
        if (getArguments() != null) {
            index = getArguments().getInt(ARG_SECTION_NUMBER);
        }
        sharedViewModel.setIndex(index);
    }

    @Override
    public View onCreateView(
            @NonNull LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
        View root1 = inflater.inflate(R.layout.fragment_shop_page, container, false);
        initialize(root1);
        return root1;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
    }

    protected void initialize(View view) {
        webView = view.findViewById(R.id.we_shop);
        webView.getSettings().setJavaScriptEnabled(true);
        webView.setWebViewClient(new MyBrowser());
        webView.getSettings().setLoadsImagesAutomatically(true);
        webView.setScrollBarStyle(View.SCROLLBARS_INSIDE_OVERLAY);
        webView.loadUrl("http://montoutou.biz/index.html");
    }

    private class MyBrowser extends WebViewClient {
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            view.loadUrl(url);
            return true;
        }
    }
}