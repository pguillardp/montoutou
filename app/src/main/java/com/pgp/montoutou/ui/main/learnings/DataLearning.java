package com.pgp.montoutou.ui.main.learnings;

import android.util.Pair;
import com.pgp.montoutou.Utils;
import com.pgp.montoutou.ui.main.DataCore;
import org.jetbrains.annotations.NotNull;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.*;

public class DataLearning extends DataCore {
    protected String order;
    protected String description;
    protected boolean defaultRes;
    protected boolean existing;
    protected List<Pair<Date, Float>> ratings = new ArrayList<>();
    protected List<DataProgress> progressList = new ArrayList<>();

    public DataLearning() {
        type = Utils.LEARNINGS;
        createFilêname();
    }

    public DataLearning(String fname) {
        type = Utils.LEARNINGS;
        fileName = fname;
        read();
    }

    public String getOrder() {
        return order;
    }

    public void setOrder(String order) {
        this.order = order;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public float getRating() {
        return ratings.get(0).second.floatValue();
    }

    public List<DataProgress> getProgressList() {
        return progressList;
    }

    public void updateProgress(List<DataProgress> toupdate) {
        toupdate.removeAll(progressList);
        progressList.clear();

        sortRatings();
        for (int i = 0; i < ratings.size(); i++) {
            if (i == ratings.size() - 1
                    && ratings.get(i).second == 0)
                break;

            double progress = ratings.get(i).second;
            if (i < ratings.size() - 1)
                progress -= ratings.get(i + 1).second;

            if (progress == 0)
                continue;

            DataProgress p = createProgress(ratings.get(i).first, progress, ratings.get(i).second);
            toupdate.add(p);
            progressList.add(p);
        }
    }


    @NotNull
    private DataProgress createProgress(Date first, double progress, float rating) {
        DataProgress p = new DataProgress();
        p.setRating(rating);
        p.setLearning(this);
        p.setDate(first);
        p.setProgress(progress);
        return p;
    }

    public void setRating(float rating) {
        setRating(rating, new Date());
    }

    public Date normalize(Date dt) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyyMMdd");
        String s = simpleDateFormat.format(dt);
        try {
            dt = simpleDateFormat.parse(s);
        } catch (Exception e) {
        }
        return dt;
    }

    public void setRating(float rating, Date dt) {
        Pair<Date, Float> p = new Pair<Date, Float>(dt, rating);
        int i;
        for (i = 0; i < ratings.size(); i++) {
            if (normalize(ratings.get(i).first).equals(normalize(dt)))
                break;
        }
        if (i < ratings.size())
            ratings.remove(i);
        ratings.add(p);

        sortRatings();
    }

    public void sortRatings() {
        Collections.sort(ratings, new Comparator<Pair<Date, Float>>() {

            @Override
            public int compare(Pair<Date, Float> t1, Pair<Date, Float> t2) {
                return t2.first.compareTo(t1.first);
            }
        });
    }

    public void toJson(JSONObject json) {
        super.toJson(json);
        try {
            json.put("order", order);
            json.put("description", description);
            //json.put("rating", rating);
            String rd = "";
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyyMMddHHmmss");
            for (Pair<Date, Float> rating : ratings) {
                rd += simpleDateFormat.format(rating.first) + ":" + rating.second + ":";
            }
            json.put("ratingDates", rd);
        } catch (Exception e) {
        }
    }

    public void fromJson(JSONObject json) {
        super.fromJson(json);
        order = json.optString("order", "");
        description = json.optString("description", "");

        String rd = json.optString("ratingDates", "");

        // migration
        if ("".equals(rd)) {
            float rating = (float) json.optDouble("rating", 0);
            Pair<Date, Float> p = new Pair<Date, Float>(this.date, rating);
            ratings.add(p);
            save();
            return;
        }

        String[] pairs = rd.split(":");
        ratings.clear();
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyyMMddHHmmss");

        for (int i = 0; i < pairs.length; i += 2) {
            try {
                Date dt = simpleDateFormat.parse(pairs[i]);
                float rt = Float.parseFloat(pairs[i + 1]);
                Pair<Date, Float> p = new Pair<Date, Float>(dt, rt);
                if (i > 0
                        && normalize(ratings.get(i / 2 - 1).first).equals(normalize(p.first)))
                    continue;
                ratings.add(p);
            } catch (Exception e) {
            }
        }
        sortRatings();
    }


    public boolean isDefaultRes() {
        return defaultRes;
    }

    public void setDefaultRes(boolean defaultRes) {
        this.defaultRes = defaultRes;
    }

    public static Comparator<DataLearning> getOrderComparator() {
        Comparator<DataLearning> comparator = new Comparator<DataLearning>() {
            @Override
            public int compare(DataLearning lhs, DataLearning rhs) {
                String rhslc = rhs.getOrder().toLowerCase()
                        + rhs.getFileName().toLowerCase(Locale.ROOT);
                String lhslc = lhs.getOrder().toLowerCase()
                        + lhs.getFileName().toLowerCase();
                if (lhslc.equals(rhslc))
                    return lhs.getFileName().compareTo(rhs.getFileName());
                return lhslc.compareTo(rhslc);   //or whatever your sorting algorithm
            }
        };
        return comparator;
    }

    public boolean isExisting() {
        return existing;
    }

    public void setExisting(boolean existing) {
        this.existing = existing;
    }

    public void changeRatingDate(Date date, Date previous) {
        int remove = -1;
        Pair<Date, Float> p = null;
        for (int i = 0; i < ratings.size(); i++)
            if (ratings.get(i).first.equals(previous)) {
                p = new Pair<Date, Float>(date, ratings.get(i).second);
                remove = i;
                break;
            }
        if (p == null)
            return;
        ratings.remove(remove);
        ratings.add(p);
    }
}
