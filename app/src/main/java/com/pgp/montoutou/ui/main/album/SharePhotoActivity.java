package com.pgp.montoutou.ui.main.album;

import android.content.Intent;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.ScaleGestureDetector;
import android.view.View;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProvider;
import androidx.viewpager2.widget.ViewPager2;
import com.pgp.montoutou.MainActivity;
import com.pgp.montoutou.R;
import com.pgp.montoutou.ui.main.DataCore;
import com.pgp.montoutou.ui.main.SharedViewModel;
import com.pgp.montoutou.ui.main.logs.DataLog;

import java.util.ArrayList;
import java.util.Collections;

public class SharePhotoActivity extends AppCompatActivity {
    private DataLog log = new DataLog();

    private final ArrayList<DataLog> photos = new ArrayList<>();
    private FullScreenImageAdapter adapter;
    private ViewPager2 viewPager;
    private SharedViewModel sharedViewModel;
    private ScaleGestureDetector scaleGestureDetector;
    private float mScaleFactor = 1.0f;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Bundle bundle = getIntent().getExtras();
        if (bundle == null)
            return;

        // https://www.futurelearn.com/info/courses/secure-android-app-development/0/steps/21592
        int flags = getIntent().getFlags() | Intent.FLAG_GRANT_READ_URI_PERMISSION | Intent.FLAG_GRANT_WRITE_URI_PERMISSION;
        getIntent().setFlags(flags);

        //sharedViewModel = new ViewModelProvider(MainActivity.getInstance()).get(SharedViewModel.class);
        String name = bundle.getString("current");

        sharedViewModel = new ViewModelProvider(MainActivity.getInstance()).get(SharedViewModel.class);
        for (DataLog log : sharedViewModel.getLogsLive().getValue()) {
            if (!"".equals(log.getPhoto().toString()))
                photos.add(log);
        }
        Collections.sort(photos, DataCore.getDateComparator());
        int position = 0;
        for (DataLog log : photos) {
            if (log.getFileName().equals(name)) {
                this.log = log;
                break;
            }
            position++;
        }
        if (position >= photos.size())
            position = photos.size() - 1;
        setContentView(R.layout.activity_fullscreen_view);
        viewPager = findViewById(R.id.pager);

        adapter = new FullScreenImageAdapter(SharePhotoActivity.this, photos);

        findViewById(R.id.btn_share).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                MainActivity.getInstance().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        MainActivity.getInstance().shareMedium(adapter.getCurrent());
                    }
                });
            }
        });

        viewPager.setAdapter(adapter);
        viewPager.setCurrentItem(position, false);

        scaleGestureDetector = new ScaleGestureDetector(this, new ScaleListener());
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onResume() {
        super.onResume();
//        for (int i = 0; i < adapter.getItemCount(); i++) {
//            adapter.g
//             holder =  recyclerView.findViewHolderForAdapterPosition(i);
//            if (holder != null)
//                holder.player.start();
//            ;
//        }
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent imageReturnedIntent) {
        super.onActivityResult(requestCode, resultCode, imageReturnedIntent);
    }


    @Override
    public boolean onTouchEvent(MotionEvent motionEvent) {
        scaleGestureDetector.onTouchEvent(motionEvent);
        return true;
    }

    private class ScaleListener extends ScaleGestureDetector.SimpleOnScaleGestureListener {
        @Override
        public boolean onScale(ScaleGestureDetector scaleGestureDetector) {
            mScaleFactor *= scaleGestureDetector.getScaleFactor();
            mScaleFactor = Math.max(0.1f, Math.min(mScaleFactor, 10.0f));
            adapter.getImageView().setScaleX(mScaleFactor);
            adapter.getImageView().setScaleY(mScaleFactor);
            return true;
        }
    }


}