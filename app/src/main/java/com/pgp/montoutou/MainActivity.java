package com.pgp.montoutou;

import android.Manifest;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;
import androidx.core.content.FileProvider;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.viewpager.widget.ViewPager;
import com.google.android.material.tabs.TabLayout;
import com.pgp.montoutou.databinding.ActivityMainBinding;
import com.pgp.montoutou.ui.main.DoggyFragment;
import com.pgp.montoutou.ui.main.SectionsPagerAdapter;
import com.pgp.montoutou.ui.main.SharedViewModel;
import com.pgp.montoutou.ui.main.ident.DataIdent;
import com.pgp.montoutou.ui.main.ident.IdentFragment;
import com.pgp.montoutou.ui.main.logs.DataLog;

import java.io.File;

// 0450 636603 31/8/22 a gallerin
// lundi 4/7 : 11h50 : irm74 - medulaire
// mercredi 6/7 : 18h30 - hopita annecy - cerebrale
// gallerin : 31/8/22 : 15h30
public class MainActivity extends AppCompatActivity {

    private ActivityMainBinding binding;
    private final int EXTERNAL_STORAGE_PERMISSION_CODE = 23;
    private static final int REQUEST_CAPTURE_IMAGE = 100;
    private static final int REQUEST_IMPORT_MEDIA = 101;
    private String currentPhotoPath;

    private static MainActivity instance = null;
    private int curposition;
    private SharedViewModel sharedViewModel;

    public static MainActivity getInstance() {
        return instance;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        int flags = getIntent().getFlags() |
                Intent.FLAG_GRANT_PERSISTABLE_URI_PERMISSION |
                Intent.FLAG_ACTIVITY_NEW_TASK |
                Intent.FLAG_GRANT_READ_URI_PERMISSION |
                Intent.FLAG_GRANT_WRITE_URI_PERMISSION;
        getIntent().setFlags(flags);

        instance = this;
        Utils.getInstance().getMonToutouFolser();

        SharedPreferences sharedPref = getPreferences(Context.MODE_PRIVATE);
        String dog = sharedPref.getString("montoutou", "Medor");
        Utils.getInstance().activity = this;

        dog = Utils.getInstance().setDogName(dog);

        binding = ActivityMainBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

        Toolbar myToolbar = findViewById(R.id.main_toolbar);
        setSupportActionBar(myToolbar);

        TabLayout tbs = findViewById(R.id.tabs);
        tbs.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                int pos = tab.getPosition() + 1;
                final SectionsPagerAdapter adapter = (SectionsPagerAdapter) binding.viewPager.getAdapter();
                if (pos >= adapter.getCount())
                    return;
                MainActivity.getInstance().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        DoggyFragment f = (DoggyFragment) adapter.getItem(tab.getPosition() + 1);
                        if (f != null)
                            f.select();
                    }
                });
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });

        SectionsPagerAdapter sectionsPagerAdapter = new SectionsPagerAdapter(this, getSupportFragmentManager());
        ViewPager viewPager = binding.viewPager;
        viewPager.setAdapter(sectionsPagerAdapter);
        TabLayout tabs = binding.tabs;
        tabs.setupWithViewPager(viewPager);

        Utils.getInstance().setContext(this, this.getApplicationContext());

        ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE},
                EXTERNAL_STORAGE_PERMISSION_CODE);

        sharedViewModel = new ViewModelProvider(MainActivity.getInstance()).get(SharedViewModel.class);

        sharedViewModel.readLearnings();
        sharedViewModel.computeProgress();

        updateSync("", "");

        //Utils.getInstance().setContext(this, getApplicationContext());
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        DoggyFragment curFragment = (DoggyFragment) Utils.getCurrentFragment(this);
        if (curFragment == null)
            return false;

        if (item.getItemId() != R.id.btnDelete)
            curFragment.setDeletionVisible(false);

        switch (item.getItemId()) {
            case R.id.btn_tb_duplicate:
                curFragment.onAdd();
                return true;

            case R.id.btnInsertMedium: {
                insertPhoto();
                return true;
            }

            case R.id.btnDelete:
                if (curFragment.isDeletionVisible()) {
                    curFragment.delete();
                    curFragment.setDeletionVisible(false);
                } else {
                    curFragment.setDeletionVisible(true);
                }
                return true;

            case R.id.btn_synchro:
                Synchro.synchronizeAsync(sharedViewModel, sharedViewModel.getDataSettings());
                return true;

            case R.id.btn_about:
                about();
                return true;

            case R.id.action_quit:
                this.finish();
                return true;

            default:
                // If we got here, the user's action was not recognized.
                // Invoke the superclass to handle it.
                return super.onOptionsItemSelected(item);

        }
    }

    private void about() {
        AlertDialog alertDialog = new AlertDialog.Builder(MainActivity.this).create();
        alertDialog.setTitle(Utils.NOM_APP);
        alertDialog.setMessage("Application de suivi de l'éducation de mes toutous.\rVersion: " + Utils.VERSION + " " + Utils.DATE_VERSION);
        alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "Ok",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
        alertDialog.show();
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onDestroy() {
        saveAll();
        super.onDestroy();
    }

    @Override
    protected void onStop() {
        super.onStop();
        killFocus();
        saveAll();
    }

    private void killFocus() {
        for (Fragment f : getSupportFragmentManager().getFragments()) {
            ((DoggyFragment) f).killFocus();
        }
    }


    @Override
    protected void onPause() {
        super.onPause();
    }


    @Override
    protected void onResume() {
        super.onResume();
    }

    private void saveAll() {
        for (Fragment f : getSupportFragmentManager().getFragments()) {
            ((DoggyFragment) f).save();
        }
    }


    protected void onActivityResult(int requestCode, int resultCode, Intent imageReturnedIntent) {
        super.onActivityResult(requestCode, resultCode, imageReturnedIntent);
        if (imageReturnedIntent == null)
            return;
        Fragment curFragment = Utils.getCurrentFragment(this);
        Uri selectedImage;
        switch (requestCode) {

            // https://developer.android.com/training/camera/photobasics
            case REQUEST_CAPTURE_IMAGE:
                Bundle extras = imageReturnedIntent.getExtras();
                Bitmap imageBitmap = (Bitmap) extras.get("data");

                Intent mediaScanIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
                File f = new File(currentPhotoPath);
                Uri contentUri = Uri.fromFile(f);
                mediaScanIntent.setData(contentUri);
                this.sendBroadcast(mediaScanIntent);
                break;

            case REQUEST_IMPORT_MEDIA:
                selectedImage = imageReturnedIntent.getData();
                String _id = RealPathUtil.getId(this, selectedImage);
                ((DoggyFragment) curFragment).addPhoto(_id);
                break;
        }
    }

    public void selectDog(String newDog) {
        if ("".equals(newDog))
            return;
        Utils.getInstance().setDogName(newDog);

        for (Fragment f : getSupportFragmentManager().getFragments()) {
            ((DoggyFragment) f).selectDog(newDog);
        }

        binding.tabs.getTabAt(0).setText(newDog);
    }


    public void updateConnected(String message) {
        MenuItem menuItem = ((Toolbar) findViewById(R.id.main_toolbar)).getMenu().findItem(R.id.btn_synchro);
        menuItem.setTitle(message);
    }

    public void updateSync(String status, String message) {
        ImageView iv = findViewById(R.id.img_sync);
        TextView tv = findViewById(R.id.txt_sync);

        if ("upload".equals(status)) {
            iv.setVisibility(View.VISIBLE);
            tv.setVisibility(View.VISIBLE);
            tv.setText(message);
            iv.setImageResource(R.drawable.sync_db);

        } else if ("download".equals(status)) {
            iv.setVisibility(View.VISIBLE);
            tv.setVisibility(View.VISIBLE);
            tv.setText(message);
            iv.setImageResource(R.drawable.sync_db);

        } else if ("delete".equals(status)) {
            iv.setVisibility(View.VISIBLE);
            tv.setVisibility(View.VISIBLE);
            tv.setText(message);
            iv.setImageResource(R.drawable.sync_db);

        } else if ("connect".equals(status)) {
            iv.setVisibility(View.VISIBLE);
            tv.setVisibility(View.VISIBLE);
            tv.setText(message);
            iv.setImageResource(R.drawable.sync_db);

        } else {
            iv.setVisibility(View.INVISIBLE);
            tv.setVisibility(View.INVISIBLE);
        }
    }

    public void shareMedium(DataLog log) {
        MediumInfo medium = log.getMediumInfo();
        File file = new File(medium.getData());

        String authority = BuildConfig.APPLICATION_ID + ".provider";
        Uri contentUri = FileProvider.getUriForFile(getInstance(), authority, file);

        Intent intent = new Intent(Intent.ACTION_SEND);
        int flags = intent.getFlags() |
                Intent.FLAG_GRANT_READ_URI_PERMISSION |
                Intent.FLAG_GRANT_WRITE_URI_PERMISSION;
        intent.setFlags(flags);
        intent.putExtra(Intent.EXTRA_STREAM, contentUri);
        intent.putExtra(Intent.EXTRA_TEXT, log.getText());
        intent.setType("image/*");
        startActivity(Intent.createChooser(intent, "Partage de photo de " + Utils.getInstance().getDogName()));
    }

    public SharedViewModel getSharedViewModel() {
        return sharedViewModel;
    }

    // called to update a datacore during synchronization
    public void updateDataCore(String fileName) {
        sharedViewModel.updateDataCore(fileName);
    }


    public void updateIdent(String remoteFileName) {
        DataIdent ident = sharedViewModel.getIdent();
        String _id = ident.get_id();
        if (!ident.getFileName().equals(remoteFileName)) {
            ident.delete();
            ident.setFileName(remoteFileName);
        }
        ident.read();
        ident.set_id(_id);
        ident.save();
        sharedViewModel.getIdentMutableLiveData().setValue(null);
        sharedViewModel.getIdentMutableLiveData().setValue(ident);
    }

    public void insertPhoto() {
        DoggyFragment curFragment = (DoggyFragment) Utils.getCurrentFragment(this);
        if (curFragment == null)
            return;

        Intent intend = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        intend.setType("image/* video/*");
        startActivityForResult(intend, REQUEST_IMPORT_MEDIA);//one can be replaced with any action code
    }
}