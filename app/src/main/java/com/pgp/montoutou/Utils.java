package com.pgp.montoutou;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.*;
import android.content.res.Resources;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.provider.MediaStore;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.webkit.MimeTypeMap;
import android.widget.*;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import com.pgp.montoutou.ui.main.DataCore;
import com.pgp.montoutou.ui.main.DoggyFragment;
import com.pgp.montoutou.ui.main.ident.DataIdent;
import com.pgp.montoutou.ui.main.learnings.DataLearning;
import com.pgp.montoutou.ui.main.settings.DataSettings;
import org.jetbrains.annotations.Nullable;
import org.json.JSONObject;

import java.io.*;
import java.lang.reflect.Method;
import java.nio.charset.StandardCharsets;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.TimeUnit;

public class Utils {

    public static String VERSION = "1.0.a5";
    public static String DATE_VERSION = "12 juillet 2022";
    public static String NOM_APP = "MonToutou";
    public static boolean isFileName = false;

    public static final String IDENT = "IDENT";
    public static final String LOG = "LOG";
    public static final String LEARNINGS = "LEARNING";
    public static final String VETO = "VETO";
    public static final String PROGRESS = "PROGRESS";
    public static final String SETTINGS = "SETTINGS";

    public static final String FILE_DATE_PATTERN = "yyyy_MM_dd_HH_mm_ss_SSS";
    public static final String APP_FOLDER = "/MonToutou/";

    private String dogName = "???";
    private Context mContext;

    private static final Utils INSTANCE = new Utils();
    // private Context context;
    MainActivity activity;
    int mPosition = -1;
    private int selectedNote;

    /**
     * constructors & singleton
     */
    private Utils() {

    }

    public static Utils getInstance() {
        return INSTANCE;
    }


    public Context getContext() {
        return mContext;
    }

//    public void setContext(Context applicationContext) {
//        context = applicationContext;
//    }

    public void setContext(MainActivity activity, Context context) {
        this.activity = activity;
        this.mContext = context;
    }

    public String getDogName() {
        return dogName;
    }

    public String setDogName(String dogName) {

        dogName = selectCurrentDog(dogName);
        if ("".equals(dogName))
            return "";

        this.dogName = dogName;
        SharedPreferences sharedPref = activity.getPreferences(Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putString("montoutou", dogName);
        editor.apply();
        //   migrate();

        return dogName;
    }

    @Nullable
    private String selectCurrentDog(String dogName) {

        ArrayList<String> dogs = getDogFolders();
        String found = "";
        for (String s : dogs)
            if (s.equals(dogName)) {
                found = s;
                break;
            }
        if (!"".equals(found))
            return found;
        if (!dogs.isEmpty())
            return dogs.get(0);

        dogName = "";
        createDogDialog(activity, true);

        return dogName;
    }


    public ArrayList<DataLearning> getResourceLearnings() {
        ArrayList<DataLearning> items = new ArrayList<>();
        String res = "";
        try {
            res = loadFile("res.txt");
            res = res.replace("montoutou", this.dogName);
        } catch (IOException e) {
            e.printStackTrace();
        }

        String[] lst = res.split("\r\n");
        for (int i = 0; i < lst.length; i++) {
            String[] fields = lst[i].split("§");
            if (fields.length < 2)
                continue;

            DataLearning l = new DataLearning();
            l.setOrder(fields[0]);
            l.setDescription(fields[1]);
            if (fields.length > 3) {
                l.setDefaultRes("def".equals(fields[2]));
            }
            l.setRating(0);

            // ms filename...
            try {
                wait(2);
            } catch (Exception r) {
            }

            items.add(l);
        }
        Collections.sort(items, DataLearning.getOrderComparator());
        return items;
    }

    public void createDog(String dogName) {
        if (getDogFolser(dogName).exists()) {
            setDogName(dogName);
            return;
        }

        getDogFolser(dogName, true);
        setDogName(dogName);

        // ident
        DataIdent idn = new DataIdent();
        idn.setBirthDate(new Date());
        idn.save();

        // default params
        DataSettings settings = new DataSettings();
        JSONObject params = new JSONObject();
        settings.setRepoUrl("http://");
        settings.save();
    }

//    public static File getDowloadFolder() {
//        String folder = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS).toString();
//        File f = new File((folder));
//        boolean b = f.exists();
//        return f;
//    }

    public static File getDocumentFolder() {
        //File folder = new File(Environment.getExternalStorageDirectory() + "/Documents");
        String data = MainActivity.getInstance().getFilesDir().toString();
        File folder = MainActivity.getInstance().getFilesDir();
        if (!folder.exists())
            folder.mkdir();
        return folder;
    }

    public File getMonToutouFolser() {
        File folder = getDocumentFolder();
        File file = new File(folder, APP_FOLDER);
        if (!file.exists())
            file.mkdir();
        return file;
    }

    public File getDogFolser() {
        return getDogFolser(dogName, false);
    }


    public File getDogFolser(String dogName) {
        return getDogFolser(dogName, false);
    }


    public File getDogSyncFoler() {
        File file = new File(getDogFolser(), "sync");
        return file;
    }


    public File getDogPhotoFolder() {
        //File file = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS);
        File file = new File(getDogFolser(), "photos");
        return file;
    }


    public File getDogFolser(String dogName, boolean create) {
        File folder = getMonToutouFolser();
        File file = new File(folder, dogName);

        if (create && !file.exists()) {
            file.mkdir();
            file = new File(folder, dogName + "/sync");
            if (!file.exists())
                file.mkdir();
            file = new File(folder, dogName + "/photos");
            if (!file.exists())
                file.mkdir();
        }

        file = new File(folder, dogName);
        return file;
    }


    public void clearEvents() {
        String[] folderFiles = getDogFolser().list();
        if (folderFiles != null) {
            for (String s : folderFiles) {
                deleteFile(s);
            }
        }
    }

    public void deleteFile(String fname) {
        File f = new File(getDogFolser(), fname);
        f.delete();

        // sync ?
        if (!getDogSyncFoler().exists())
            return;
        f = new File(getDogSyncFoler(), fname);
        try {
            f.createNewFile();
        } catch (Exception e) {
        }
    }


    public void saveJson(String fileName, String type, JSONObject json) {
        if (!getDogFolser(dogName).exists())
            return;
        File file = new File(getDogFolser(), fileName);
        writeTextData(file, json.toString());
        file.setLastModified(new Date().getTime());
    }


    // writeTextData() method save the data into the file in byte format
    // It also toast a message "Done/filepath_where_the_file_is_saved"
    public void writeTextData(File file, String data) {
        FileOutputStream fileOutputStream = null;
        try {
            fileOutputStream = new FileOutputStream(file);
            fileOutputStream.write(data.getBytes());
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (fileOutputStream != null) {
                try {
                    fileOutputStream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public ArrayList<String> getDogFolders() {
        final ArrayList<String> files = new ArrayList<>();
        String[] dogfiles = getMonToutouFolser().list();
        if (dogfiles == null)
            return files;
        for (String s : dogfiles) {
            files.add(s);
        }
        Collections.sort(files, new Comparator<String>() {
            @Override
            public int compare(String s, String t1) {
                return s.toLowerCase().compareTo(t1.toLowerCase());
            }
        });
        return files;
    }

    public List<String> getEvtList() {
        final List<String> files = new ArrayList<>();
        String[] jsonFiles = getDogFolser().list();
        if (jsonFiles == null)
            return files;

        for (String s : jsonFiles) {
            files.add(s);
        }

        //Collections.sort(sortedLevel, String.CASE_INSENSITIVE_ORDER);
        Collections.sort(files, new Comparator<String>() {
            @Override
            public int compare(String s, String t1) {
                return s.toLowerCase().compareTo(t1.toLowerCase());
            }
        });

        return files;
    }


    //load file from apps res/raw folder or Assets folder
    public String loadFile(String fileName) throws IOException {
        // default  learning
        // http://www.41post.com/3985/programming/android-loading-files-from-the-assets-and-raw-folders
//        Resources resources = mContext.getResources();

        String output = "";
        try {
            //Load the file from the raw folder - don't forget to OMIT the extension
            output = loadFile(fileName, true);
        } catch (IOException e) {
            return "";
        }

        if (!"".equals(output))
            return output;

        try {
            //Load the file from assets folder - don't forget to INCLUDE the extension
            output = loadFile(fileName, false);
        } catch (IOException e) {
            return "";
        }
        return output;
    }


    public String loadFile(String fileName, boolean loadFromRawFolder) throws IOException {
        Resources resources = mContext.getResources();
        InputStream iS;

        if (loadFromRawFolder) {
            iS = resources.openRawResource(R.raw.learnings);
        } else {
            iS = resources.getAssets().open(fileName);
        }

        InputStreamReader reader = null;
        String res = "";
        try {
            reader = new InputStreamReader(iS, StandardCharsets.UTF_8);
            int i = -1;
            StringBuffer buffer = new StringBuffer();
            while ((i = reader.read()) != -1) {
                buffer.append((char) i);
            }
            res = buffer.toString();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (reader != null)
                reader.close();
            if (iS != null) {
                try {
                    iS.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            return res;
        }
    }

    public String readFromFile(String dog, final String fileName) {
        File f = new File(getDogFolser(dog), fileName);

        // web read vs local smartphone
        String content = getdata(f);

        return content;
    }


    public JSONObject jsonFileContent(String fileName) {
        if (!fileName.toLowerCase(Locale.ROOT).endsWith(".json"))
            fileName = fileName + ".json";
        String json = getInstance().readFromFile(dogName, fileName);
        try {
            return new JSONObject(json);
        } catch (Exception e) {
        }
        return new JSONObject();
    }

    public void createDogDialog(Context c, boolean finish) {
        final EditText taskEditText = new EditText(c);
        AlertDialog dialog = new AlertDialog.Builder(c)
                .setTitle("Nouveau Toutou")
                .setMessage("Nom du Toutou?")
                .setView(taskEditText)
                .setPositiveButton("Creer", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        String toutou = String.valueOf(taskEditText.getText());
                        createDog(toutou);
                        MainActivity.getInstance().selectDog(toutou);
                    }
                })
                .setNegativeButton("Annuler", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        if (finish)
                            activity.finish();
                    }
                })
                .create();
        dialog.show();
    }

    // getdata() is the method which reads the data
    // the data that is saved in byte format in the file
    public String getdata(File myfile) {
        FileInputStream fileInputStream = null;
        try {
            fileInputStream = new FileInputStream(myfile);
            InputStreamReader reader = new InputStreamReader(fileInputStream, StandardCharsets.UTF_8);
            int i = -1;
            StringBuffer buffer = new StringBuffer();
            while ((i = reader.read()) != -1) {
                buffer.append((char) i);
            }
            return buffer.toString();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (fileInputStream != null) {
                try {
                    fileInputStream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return null;
    }


    public void setChangeListener(EditText ed, DataCore datamodel, String method) {
        setChangeListener(ed, datamodel, method, null);
    }

    public void setChangeListener(EditText ed, DataCore datamodel, String method, DoggyFragment sort) {
        ed.addTextChangedListener(new TextWatcher() {
            public void afterTextChanged(Editable s) {
                ed.setTag("mod");
            }

            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }
        });
        ed.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean b) {
                if (!b && !"".equals(ed.getTag())) {
                    try {
                        Class[] cArg = new Class[1];
                        cArg[0] = String.class;
                        Method m = datamodel.getClass().getMethod(method, cArg);
                        String value = ((EditText) view).getText().toString();
                        m.invoke(datamodel, value);
                        datamodel.save();
                        if (sort != null)
                            sort.sortAdapter();
                    } catch (Exception e) {
                    }
                }
                ed.setTag("");
            }
        });
    }

    private void dateToViews(DoggyFragment fragment, DataCore datamodel, TextView tvDate, String property) {

        String getter = "get" + property.substring(0, 1).toUpperCase()
                + property.substring(1);
        String setter = "set" + property.substring(0, 1).toUpperCase()
                + property.substring(1);

        Date dt = new Date();
        try {
            Class[] cArg = new Class[1];
            cArg[0] = Date.class;
            Method m = datamodel.getClass().getMethod(getter, null);
            dt = (Date) m.invoke(datamodel);
            // !datamodel.save();
        } catch (Exception e) {
        }

        Date finalDt = dt;
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy");
        String s = simpleDateFormat.format(finalDt);
        tvDate.setText(s);

        tvDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View vw) {
                Calendar calendar = Calendar.getInstance();
                calendar.setTime(finalDt);
                int year = calendar.get(Calendar.YEAR);
                int month = calendar.get(Calendar.MONTH);
                int day = calendar.get(Calendar.DAY_OF_MONTH);

                DatePickerDialog datePickerDialog = new DatePickerDialog(activity,
                        new DatePickerDialog.OnDateSetListener() {
                            @Override
                            public void onDateSet(DatePicker pickview, int year,
                                                  int monthOfYear, int dayOfMonth) {

                                Date newdate = DataCore.changeDateDay(finalDt, year, monthOfYear + 1, dayOfMonth);
                                try {
                                    Class[] cArg = new Class[1];
                                    cArg[0] = Date.class;
                                    Method m = datamodel.getClass().getMethod(setter, cArg);
                                    m.invoke(datamodel, newdate);
                                    datamodel.save();
                                } catch (Exception e) {
                                }

                                //dc.setDateDay(year, monthOfYear + 1, dayOfMonth);
                                dateToViews(fragment, datamodel, tvDate, property);
                                fragment.sortAdapter();
                            }
                        }, year, month, day);

                datePickerDialog.show();
            }
        });
    }


    public void manageDatePicker(DoggyFragment fragment, DataCore dc, TextView tvDate, String property) {
        tvDate.setKeyListener(null);
        dateToViews(fragment, dc, tvDate, property);
    }


    public void manageDatePicker(DoggyFragment fragment, DataCore dc, TextView tvDate) {
        manageDatePicker(fragment, dc, tvDate, "date");
    }


    private void timeToViews(DataCore dc, TextView tvTime) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("HH:mm");
        tvTime.setText(simpleDateFormat.format(dc.getDate()));
    }

    public void manageTimePicker(DoggyFragment fragment, DataCore dc, TextView tvTime) {

        if (tvTime != null)
            tvTime.setKeyListener(null);
        timeToViews(dc, tvTime);

        if (tvTime != null) {

            tvTime.setKeyListener(null);
            tvTime.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View vw) {
                    TimePickerDialog timePickerDialog = new TimePickerDialog(activity,
                            new TimePickerDialog.OnTimeSetListener() {
                                @Override
                                public void onTimeSet(TimePicker pickview, int hourOfDay,
                                                      int minute) {
                                    dc.setDateTime(hourOfDay, minute);
                                    timeToViews(dc, tvTime);
                                    dc.save();
                                    fragment.sortAdapter();
                                }

                            }, dc.getDate().getHours(), dc.getDate().getMinutes(), true);

                    timePickerDialog.show();
                }
            });
        }
    }

    public void manageDeleteCheckBox(DataCore log, CheckBox delchk) {
        delchk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                log.setSelected(delchk.isChecked());
            }
        });
    }

    public static Fragment getCurrentFragment(FragmentActivity activity) {
        if (activity == null)
            return null;

        FragmentManager manager = activity.getSupportFragmentManager();
        List<Fragment> fragmentList = manager.getFragments();
        if (fragmentList != null) {
            for (int i = 0; i < fragmentList.size(); i++) {
                Fragment aFragmentList = fragmentList.get(i);
                if (aFragmentList != null && aFragmentList.isVisible() && aFragmentList.isAdded() && aFragmentList.getUserVisibleHint())
                    return aFragmentList;
            }/* www.ja v a  2  s  .c  om*/
        }
        return null;
    }


    public String getAge(Date birthDate) {
        String age = "";
        if (birthDate == null)
            return age;

        long diffInMillies = Math.abs((new Date()).getTime() - birthDate.getTime());
        long diff = TimeUnit.DAYS.convert(diffInMillies, TimeUnit.MILLISECONDS) / 30;
        if (diff < 12)
            age = String.format("%d mois", diff);
        else if (diff <= 18 && (diff % 12 == 0))
            age = String.format("%d ans", diff / 12);
        else if (diff <= 18)
            age = String.format("%d ans %ld mois", diff / 12, diff % 12);
        else
            age = String.format("%d ans", diff / 12);
        return age;
    }

    public static void setTextEndCursor(EditText ed, String text) {
        ed.setText("");
        ed.append(text);
    }

    private Cursor queryImages(Context context, String _id) {

        String sel = MediaStore.Images.Media._ID + "=?";

        Cursor cursor = context.getContentResolver().query(MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
                new String[]{MediaStore.MediaColumns._ID,
                        MediaStore.MediaColumns.DATA,
                        MediaStore.MediaColumns.DATE_ADDED,
                        MediaStore.Images.ImageColumns.MIME_TYPE,
                        MediaStore.Images.ImageColumns.ORIENTATION}, sel, new String[]{_id}, null, null);

        if (null == cursor || !cursor.moveToFirst()) {
            if (cursor != null)
                cursor.close();
            return null;
        }
        return cursor;
    }


    private Cursor queryVideos(Context context, String _id) {
        String sel = MediaStore.Video.Media._ID + "=?";

        Cursor cursor = context.getContentResolver().query(MediaStore.Video.Media.EXTERNAL_CONTENT_URI,
                new String[]{MediaStore.MediaColumns._ID,
                        MediaStore.MediaColumns.DATA,
                        MediaStore.MediaColumns.DATE_ADDED,
                        MediaStore.Video.VideoColumns.MIME_TYPE}, sel, new String[]{_id}, null, null);

        if (null == cursor || !cursor.moveToFirst()) {
            if (cursor != null)
                cursor.close();
            return null;
        }
        return cursor;
    }


    public MediumInfo getMediumInfo(Context context, String _id) {
        MediumInfo info = new MediumInfo();
        boolean isImage = true;

        if ("".equals(_id) || _id == null)
            return info;

        String sel = MediaStore.Images.Media._ID + "=?";

        Cursor cursor = queryImages(context, _id);
        if (cursor == null) {
            cursor = queryVideos(context, _id);

            // means: reference lost...
            if (cursor == null)
                return info;
            isImage = false;
        }

        info.setData(cursor.getString(1));
        info.setDateTaken(new java.util.Date(cursor.getLong(2) * 1000L));
        info.setMime(cursor.getString(3));
        if (isImage)
            info.setOrientation(cursor.getInt(4));
        cursor.close();

        long _lid = Long.parseLong(_id);

        // image
        if (isImage) {
            cursor = MediaStore.Images.Thumbnails.queryMiniThumbnail(context.getContentResolver(),
                    _lid, MediaStore.Images.Thumbnails.MINI_KIND, null);
            if (cursor != null && cursor.getCount() > 0) {
                cursor.moveToFirst();
                info.setThumbnail(Uri.parse(cursor.getString(cursor.getColumnIndexOrThrow(MediaStore.Images.Thumbnails.DATA))));
                cursor.close();
            } else {
                Bitmap bitmap = MediaStore.Images.Thumbnails.getThumbnail(
                        context.getContentResolver(),
                        _lid,
                        MediaStore.Images.Thumbnails.MINI_KIND,
                        null);
                info.setThumbBitmap(bitmap);
            }

            // video
        } else {

        }

        return info;
    }

    public static String fileNameToMediumName(String name) {
        String[] fs = name.split("_");
        String mediumName = fs[0] + fs[1] + fs[2] + "_" + fs[3] + fs[4] + fs[5] + fs[6];
        fs = name.split("\\.");
        mediumName += "." + fs[1];
        return mediumName;
    }

    public static String addToPictureOrVideoAlbum(String filePath) {

        MimeTypeMap mime = MimeTypeMap.getSingleton();
        String ext = MimeTypeMap.getFileExtensionFromUrl(filePath);
        String mm = mime.getMimeTypeFromExtension(ext);

        String[] fs = filePath.split(File.separator);
        String name = fs[fs.length - 1];
        String savedImageURL = "";
        String _id = "";

        try {

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {

                String target = Environment.DIRECTORY_PICTURES;
                ContentResolver resolver = getInstance().mContext.getContentResolver();
                ContentValues contentValues = new ContentValues();
                contentValues.put(MediaStore.MediaColumns.DISPLAY_NAME, name);
                contentValues.put(MediaStore.MediaColumns.MIME_TYPE, mm);
                contentValues.put(MediaStore.MediaColumns.RELATIVE_PATH, target);

                // video
                Uri imageUri;
                if (mm.startsWith("video")) {

                    //Save the name and description of a video in a ContentValues map.
                    ContentValues values = new ContentValues(2);
                    values.put(MediaStore.Video.Media.MIME_TYPE, "video/mp4");

                    //Add a new record (identified by uri) without the video, but with the values just set.
                    imageUri = resolver.insert(MediaStore.Video.Media.EXTERNAL_CONTENT_URI, values);

                    //Now get a handle to the file for that record, and save the data into it.
                    try {
                        InputStream is = new FileInputStream(filePath);
                        OutputStream os = resolver.openOutputStream(imageUri);
                        byte[] buffer = new byte[4096]; //tweaking this number may increase performance
                        int len;
                        while ((len = is.read(buffer)) != -1) {
                            os.write(buffer, 0, len);
                        }
                        os.flush();
                        is.close();
                        os.close();
                    } catch (Exception e) {
                        System.out.println("exception while writing video: " + e.getMessage());
                    }

                    // default: image
                } else {

                    OutputStream fos;
                    Bitmap bitmap = BitmapFactory.decodeFile(filePath);

                    ExifInterface exif = new ExifInterface(filePath);
                    int orientation = exifToDegrees(exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, 1));

                    Matrix matrix = new Matrix();
                    matrix.postRotate(orientation);

                    bitmap = Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), matrix, true);
                    imageUri = resolver.insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, contentValues);

                    if ("image/jpeg".equals(mm)) {
                        fos = resolver.openOutputStream(Objects.requireNonNull(imageUri));
                        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, fos);
                        Objects.requireNonNull(fos);
                    }
                }

                savedImageURL = imageUri.toString();

            } else {

                //savedImageURL = MediaStore.Images.Media.insertImage(getInstance().mContext.getContentResolver(), bitmap, name, "");
            }

        } catch (Exception e) {
            System.out.println(e.getMessage());
        }

        String[] sf = savedImageURL.split(File.separator);
        _id = sf[sf.length - 1];

        return _id;
    }

    public static int exifToDegrees(int exifOrientation) {
        int rotation;
        switch (exifOrientation) {
            case ExifInterface.ORIENTATION_ROTATE_90:
            case ExifInterface.ORIENTATION_TRANSPOSE:
                rotation = 90;/*from   w ww  .  j a va  2s .c om*/
                break;
            case ExifInterface.ORIENTATION_ROTATE_180:
            case ExifInterface.ORIENTATION_FLIP_VERTICAL:
                rotation = 180;
                break;
            case ExifInterface.ORIENTATION_ROTATE_270:
            case ExifInterface.ORIENTATION_TRANSVERSE:
                rotation = 270;
                break;
            default:
                rotation = 0;
        }
        return rotation;
    }

    public void migrate() {
        File dst = getDogFolser();
        File src = new File(Environment.getExternalStorageDirectory() + "/Documents/MonToutou/" + dogName);
        try {
            copyFiles(src, dst);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void copyFiles(File sourceLocation, File targetLocation)
            throws IOException {

        if (sourceLocation.isDirectory()) {
            if (!targetLocation.exists()) {
                targetLocation.mkdir();
            }

            String[] children = sourceLocation.list();
            for (int i = 0; i < sourceLocation.listFiles().length; i++) {

                copyFiles(new File(sourceLocation, children[i]),
                        new File(targetLocation, children[i]));
            }
        } else {

            long mod = sourceLocation.lastModified();
            InputStream in = new FileInputStream(sourceLocation);

            OutputStream out = new FileOutputStream(targetLocation);

            // Copy the bits from instream to outstream
            byte[] buf = new byte[1024];
            int len;
            while ((len = in.read(buf)) > 0) {
                out.write(buf, 0, len);
            }
            in.close();
            out.close();

            targetLocation.setLastModified(mod);
        }
    }
}

