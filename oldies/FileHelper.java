package com.pgp.montoutou;

import android.Manifest;
import android.content.Context;
import android.os.Environment;
import android.text.TextUtils;
import android.widget.Toast;
import androidx.core.app.ActivityCompat;
import org.json.JSONObject;

import java.io.*;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.ExecutionException;

public class FileHelper {
    private static final String MAP_DAT = "properties.dat";
    public static final int MAX_PROPERTY = 16;
    public static final String FILE_DATE_PATTERN = "yyyy_MM_dd_HH_mm_ss_SSS";
    private static FileHelper INSTANCE = new FileHelper();
    public static final String APP_FOLDER = "/MonToutou/";
    final public static String TXT_EXT = ".txt";
    public static final String PNG_EXT = ".png";
    public static final String PROP_SEPARATOR = "#";
    public static final int MAX_NAME_LENGTH = 64;
    public static final String EDITABLE = "E";

    private boolean debug = false;

    private HashMap<String, String> webLevelMap = new HashMap<>();
    private HashMap<String, String> localLevelMap = new HashMap<>();
    private Context context;


    /**
     * constructors & singleton
     */
    private Utils() {

    }

    public static Utils getInstance() {
        return INSTANCE;
    }


    public void setContext(Context applicationContext) {
        context = applicationContext;
    }

    /**
     * check basic level definitions are present
     */
    public String getEvtFolder() {
        File folder = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOCUMENTS);
        return "";
    }

    public void createNoteFolser() {
        File folder = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOCUMENTS);
        File file = new File(folder, APP_FOLDER);
        if (!file.exists())
            file.mkdir();
    }

    public void createDogFolser(String name) {
        File folder = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOCUMENTS);
        File file = new File(folder, APP_FOLDER + "/" + name);
        if (!file.exists())
            file.mkdir();
    }


    public boolean folderExists(String dogName) {
        File folder = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOCUMENTS);
        File file = new File(folder, APP_FOLDER + "/" + dogName);
        return file.exists();
    }

    public String datePrefix(Date date) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(FILE_DATE_PATTERN);
        return simpleDateFormat.format(date);
    }


    public Date fileNameToDate(String fname) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(FILE_DATE_PATTERN);
        Date date = new Date();
        try {
            fname = fname.substring(0, FILE_DATE_PATTERN.length() - 1);
            date = simpleDateFormat.parse(fname);
            return date;
        } catch (Exception e) {
        }
        return date;
    }


    public String fileNotePrefix(Date date, String type, boolean delete) {
        return datePrefix(date) + "_"
                + type.toUpperCase() + "_"
                + (delete ? "DEL" : "");
    }


    public String saveNote(String dog, Date date, String type, String content, boolean delete) {
        File folder = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOCUMENTS);
        String fname = fileNotePrefix(date, type, delete) + ".json";
        File file = new File(folder, APP_FOLDER + dog + "/" + fname);
        writeTextData(file, content);
        return fname;
    }


    public void saveJson(String dogName, String fileName, String type, JSONObject json) {
        File folder = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOCUMENTS);
        File file = new File(folder, APP_FOLDER + dogName + "/" + fileName);
        writeTextData(file, json.toString());
    }


    public void clearEvents(String dogName) {
        File folder = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOCUMENTS);
        File f = new File(folder, APP_FOLDER + dogName);
        String[] folderFiles = f.list();
        if (folderFiles != null) {
            for (String s : folderFiles) {
                deleteFile(dogName, s);
            }
        }
    }

    public void deleteFile(String dogName, String fname) {
        File folder = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOCUMENTS);
        File f = new File(folder, APP_FOLDER + dogName + "/" + fname);
        f.delete();
    }

    // writeTextData() method save the data into the file in byte format
    // It also toast a message "Done/filepath_where_the_file_is_saved"
    private void writeTextData(File file, String data) {
        FileOutputStream fileOutputStream = null;
        try {
            fileOutputStream = new FileOutputStream(file);
            fileOutputStream.write(data.getBytes());
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (fileOutputStream != null) {
                try {
                    fileOutputStream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    /*
     * return list of current levels
     *
     * @return
     */
    public List<String> getEvtList(String dogname) {
        File folder = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOCUMENTS);
        File f = new File(folder, APP_FOLDER + "/" + dogname);
        final List<String> files = new ArrayList<>();
        String[] jsonFiles = f.list();
        if (f == null)
            return files;

        for (String s : jsonFiles) {
            files.add(s);
        }

        //Collections.sort(sortedLevel, String.CASE_INSENSITIVE_ORDER);
        Collections.sort(files, new Comparator<String>() {
            @Override
            public int compare(String s, String t1) {
                return s.toLowerCase().compareTo(t1.toLowerCase());
            }
        });

        return files;
    }


    /**
     * read from file. Note: file extension is managed separately
     *
     * @return
     */
    public String readFromFile(String dog, final String levelName) {
        String content = "";
        File folder = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOCUMENTS);
        File f = new File(folder, APP_FOLDER + "/" + dog + "/" + levelName);

        // web read vs local smartphone
        content = getdata(f);

        return content;
    }

    // getdata() is the method which reads the data
    // the data that is saved in byte format in the file
    private String getdata(File myfile) {
        FileInputStream fileInputStream = null;
        try {
            fileInputStream = new FileInputStream(myfile);
            int i = -1;
            StringBuffer buffer = new StringBuffer();
            while ((i = fileInputStream.read()) != -1) {
                buffer.append((char) i);
            }
            return buffer.toString();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (fileInputStream != null) {
                try {
                    fileInputStream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return null;
    }


    /**
     * find a file from path in a file list
     *
     * @param file
     * @param txtFiles
     * @return
     */
    private boolean findFile(String file, List<String> txtFiles) {
        if (txtFiles == null) {
            return false;
        }
        for (String s : txtFiles) {
            if (s.equalsIgnoreCase(file + TXT_EXT)) {
                return true;
            }
        }
        return false;
    }


    /**
     * create default level folder and missing default level files (if any)
     */
    public void defaultFolder(String[] defLevels) {

        // check level folder exists & check its content
        String targetPath = getEvtFolder();
        targetPath = targetPath.substring(0, targetPath.length() - 1);
        File targetDirector = new File(targetPath);
        boolean b = false;
        if (targetDirector.exists() == false) {
            b = targetDirector.mkdirs();
        }
        targetDirector = new File(targetPath + "/help");
        if (targetDirector.exists() == false) {
            b = targetDirector.mkdirs();
        }
        targetDirector = new File(targetPath + "/medor");
        if (targetDirector.exists() == false) {
            b = targetDirector.mkdirs();
        }

        List<String> txtFiles = getEvtList(TXT_EXT);

        for (int i = defLevels.length - 1; i >= 0; i -= 2) {

            // create definition file write protected
            if (findFile(defLevels[i - 1], txtFiles) == false) {
                //saveToTxtFile(defLevels[i - 1] + Utils.TXT_EXT, defLevels[i]);
                String properties = this.getProperties(defLevels[i - 1], false);
                this.setEditable(properties, false);
            }
        }

        targetDirector = new File(targetPath + "/debug.dat");
        if (targetDirector.exists() == false) {
            debug = true;
        } else {
            debug = false;
        }
    }

    /**
     * return list of missing png files
     *
     * @return
     */
    List<String> getMissingPng() {
        List<String> pngFiles = new ArrayList<>(), txtFiles = getEvtList(TXT_EXT);

        // get missing png
        for (String s : txtFiles) {
            String fname = this.shortNameToTxtFull(s);
            fname = fname.replace(TXT_EXT, PNG_EXT);
            File fpng = new File(fname);
            if (!fpng.exists()) {
                pngFiles.add(fname);
            }
        }
        return pngFiles;
    }

    /**
     * clear png files without txt files
     */
    public void deletePngWithoutTxt() {
        List<String> pngFiles = getEvtList(PNG_EXT);

        // get missing png
        for (String s : pngFiles) {
            String fname = s.replace(PNG_EXT, TXT_EXT);
            File ftxt = new File(getEvtFolder() + fname);
            if (!ftxt.exists()) {
                File fpng = new File(getEvtFolder()
                        + fname.replace(TXT_EXT, PNG_EXT));
                fpng.delete();
            }
        }
    }


    public String shortNameToTxtFull(String filename) {
        String fullname = new String(filename);
        fullname = fullname.replace(PNG_EXT, TXT_EXT);
        if (fullname.contains(TXT_EXT) == false) {
            fullname += TXT_EXT;
        }
        if (fullname.contains(getEvtFolder()) == false) {
            fullname = getEvtFolder() + fullname;
        }
        return fullname;
    }


    public String shortNameToPngFull(String filename) {
        String fullname = shortNameToTxtFull(filename);
        fullname = fullname.replace(TXT_EXT, PNG_EXT);
        return fullname;
    }

    public String fullNameToShort(String filename) {
        return filename.replace(TXT_EXT, "")
                .replace(PNG_EXT, "")
                .replace(getEvtFolder(), "");
    }

    /**
     * save level on disk - assumes fileName exists
     *
     * @param fileName
     */
    /*public void saveToTxtFile(String fileName, String fileContent) {
        this.saveTextFile(fileName, fileContent, false, false);
    }*/
    public void saveToTmpTxtFile(String fileName, String fileContent, boolean isweb) {
        //this.saveTextFile(fileName, fileContent, true, isweb);
    }

    /**
     * save & read temporary level states
     */
    //private void saveTextFile(String fileName, String fileContent, boolean temp, boolean isweb) {
        /*if (fileName == null
                || "".equals(fileName)) {
            return;
        }

        // full path name
        String fullname = shortNameToTxtFull(fileName);
        if (temp) {
            if (isweb) {
                fullname = fullname.replace(APP_FOLDER, APP_FOLDER + "tmpweb/");
            } else {
                fullname = fullname.replace(APP_FOLDER, APP_FOLDER + "tmplocal/");
            }
        }*/
    // File folder = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOCUMENTS);
    //File file = new File(folder, APP_FOLDER + "/" +);

    // write to file
        /*BufferedWriter bout;
        try {
            bout = new BufferedWriter(new FileWriter(fullname));
            bout.write(fileContent);
            bout.flush();
            bout.close();
        } catch (IOException e) {
            e.printStackTrace();
        }*/
    //  }
    public String readTmpTxtFile(String filename, boolean isweb) {
        return readTextFile(filename, true, isweb);
    }


    private String readTextFile(final String filename, boolean temp, boolean isweb) {
        String content;
        String fullname = shortNameToTxtFull(filename);

        if (temp) {
            if (isweb) {
                fullname = fullname.replace(APP_FOLDER, APP_FOLDER + "tmpweb/");
            } else {
                fullname = fullname.replace(APP_FOLDER, APP_FOLDER + "tmplocal/");
            }
        }

        final String fnameext = filename + Utils.TXT_EXT;
        File f = null;
        StringBuilder fileContent = new StringBuilder();

        // manage properties => just keep file name
        if (!temp) {
            String prop[] = filename.split(Utils.PROP_SEPARATOR);
            if (prop.length < 12) {
                File dir = new File(getEvtFolder());

                FilenameFilter fileFilter = new FilenameFilter() {
                    public boolean accept(File dir, String name) {
                        if (name.equalsIgnoreCase(fnameext)) {
                            return true;
                        } else {
                            return false;
                        }
                    }
                };

                File[] files = dir.listFiles(fileFilter);
                if (files != null) {
                    for (File file : files) {
                        if (!file.isDirectory()) {
                            f = file;
                            break;
                        }
                    }
                }
            } else {
                f = new File(fullname);
            }
        } else {
            f = new File(fullname);
        }

        // read file if it exists
        if (f != null
                && f.exists() == true) {

            BufferedReader br;
            try {
                br = new BufferedReader(new FileReader(f));
                String line;
                try {
                    while ((line = br.readLine()) != null) {
                        fileContent.append(line + "\n");
                    }
                } catch (IOException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }

                // load

            } catch (FileNotFoundException e1) {
                e1.printStackTrace();
            }
        }

        content = fileContent.toString();
        return content;
    }

    /**
     * /**
     * delete file from its level name
     *
     * @param levelName
     * @return
     */
    public boolean deleteFile(String levelName) {
        List<String> txtFiles = getEvtList(TXT_EXT);
        boolean deleted = true;
        for (String s : txtFiles) {
            if (this.getLevelName(s).equalsIgnoreCase(levelName)) {
                File f = new File(this.getEvtFolder() + s + TXT_EXT);
                if (f.exists()) {
                    if (!f.delete()) {
                        deleted = false;
                    }
                }
            }
        }
        return deleted;
    }


    /**
     * returns next unsolved level
     *
     * @return
     * @throws ExecutionException
     * @throws InterruptedException
     */
    public String getNextLevel(String levelName, boolean web) throws InterruptedException, ExecutionException {
        String firstNotSolved = null,
                firstLevel = null;
        List<String> files;
        boolean found = false;

        files = getEvtList(Utils.TXT_EXT);

        if (files == null) {
            return new String("");
        }

        for (String nextLevel : files) {
            if (firstLevel == null) {
                firstLevel = new String(nextLevel);
            }
            if (firstNotSolved == null
                    && !this.isSolved(nextLevel)) {
                firstNotSolved = new String(nextLevel);
            }
            if (found) {
                if (!this.isSolved(nextLevel)) {
                    return nextLevel;
                }
            } else if (levelName.equalsIgnoreCase(this.getLevelName(nextLevel))) {
                found = true;
            }
        }

        if (firstNotSolved != null) {
            return firstNotSolved;
        }

        return firstLevel;
    }

    /**
     * create missing png file
     */

    public Date getModified(String fileName) {
        File file = new File(fileName);

        Date lastModified = new Date();
        if (file.exists()) {
            lastModified = new Date(file.lastModified());
        }
        return lastModified;
    }


    /**
     * manage file with properties: the file name is build as follow: property separator: #
     * - 0 file name (64 char max)
     * - 1 property syntax version: 1, 2, 3...
     * - 2 L|W: local or web
     * - 3 O|C: official or custom
     * - 4 Lasers: number of lasers
     * - 5 opticals: number of opticals
     * - 6 number of rays
     * - 7 elapsed in secs
     * - 8 number of moves
     * - 9 1|0: solved or not
     * - 10 difficulty: 1 to 999
     * - 11 field #1: future use
     * - 12 field #1: future use
     * - 13 field #1: future use
     * - 14 field #1: future use
     * - 15 field #1: future use
     */
    private String[] getFileProp(String nameWithProperties) {
        String cur[] = nameWithProperties.split(Utils.PROP_SEPARATOR),
                prop[] = new String[MAX_PROPERTY];

        prop[1] = "1";
        prop[2] = "L";
        prop[3] = "C";
        prop[4] = "0";
        prop[5] = "0";
        prop[6] = "0";
        prop[7] = "0";
        prop[8] = "0";
        prop[9] = "g";
        prop[10] = "x";
        prop[11] = "0";
        prop[12] = "";
        prop[13] = "";
        prop[14] = "";
        prop[15] = "";

        for (int i = 0; i < cur.length; i++) {
            prop[i] = cur[i];
        }

        return prop;
    }


    private String setProperty(String nameWithProperties, String property, int propIndex) {
        if (propIndex >= MAX_PROPERTY) {
            return nameWithProperties;
        }
        String cur[] = getFileProp(nameWithProperties);
        cur[propIndex] = property;
        return TextUtils.join(Utils.PROP_SEPARATOR, cur);
    }


    /**
     * save file properties => rename from old name with new name with new property values
     *
     * @param properties
     */
    public void saveProperties(String levelName, String properties, boolean isWeb) {
        if (isWeb) {
            this.webLevelMap.put(levelName, properties);
        } else {
            this.localLevelMap.put(levelName, properties);
        }
        this.saveMap();
    }

    public String getProperties(String levelName, boolean web) {
        HashMap<String, String> m;
        if (web) {
            m = this.webLevelMap;
        } else {
            m = this.localLevelMap;
        }
        String properties = (String) m.get(levelName);
        if (properties == null) {
            properties = TextUtils.join(Utils.PROP_SEPARATOR, this.getFileProp(levelName));
        }
        return properties;
    }


    /**
     * manage properties
     *
     * @param nameWithProperties
     * @return
     */
    public String getLevelName(String nameWithProperties) {
        return getFileProp(nameWithProperties)[0];
    }

    public String setFileName(String nameWithProperties, String name) {
        String newName = new String(name);
        if (newName.length() > Utils.MAX_NAME_LENGTH) {
            newName = newName.substring(0, Utils.MAX_NAME_LENGTH);
        }
        newName.replace(Utils.PROP_SEPARATOR, "_");
        return setProperty(nameWithProperties, newName, 0);
    }

    public boolean isWEB(String nameWithProperties) {
        if (getFileProp(nameWithProperties)[2].equalsIgnoreCase("W")) {
            return true;
        }
        return false;
    }

    public boolean isLocal(String nameWithProperties) {
        if (getFileProp(nameWithProperties)[2].equalsIgnoreCase("L")) {
            return true;
        }
        return false;
    }

    public String setWEB(String nameWithProperties) {
        return setProperty(nameWithProperties, "W", 2);
    }

    public String setLocal(String nameWithProperties) {
        return setProperty(nameWithProperties, "L", 2);
    }

    public String getNbLasers(String nameWithProperties) {
        return getFileProp(nameWithProperties)[4];
    }

    public String setNbLasers(String nameWithProperties, int lasers) {
        return setProperty(nameWithProperties, Integer.toString(lasers), 4);
    }

    public String getNbOpticals(String nameWithProperties) {
        return getFileProp(nameWithProperties)[5];
    }

    public String setNbOpticals(String nameWithProperties, int opticals) {
        return setProperty(nameWithProperties, Integer.toString(opticals), 5);
    }

    public String getNbRays(String nameWithProperties) {
        return getFileProp(nameWithProperties)[6];
    }

    public String setNbRays(String nameWithProperties, int rays) {
        return setProperty(nameWithProperties, Integer.toString(rays), 6);
    }

    public String getElapsed(String nameWithProperties) {
        return getFileProp(nameWithProperties)[7];
    }

    public String setElapsed(String nameWithProperties, int elapsed) {
        return setProperty(nameWithProperties, Integer.toString(elapsed), 7);
    }

    public String getElapsedHHMMSS(String nameWithProperties) {
        String s = getFileProp(nameWithProperties)[7];
        int sec = Integer.parseInt(s),
                min,
                hour;
        hour = sec / 3600;
        min = (sec - hour * 3600) / 60;
        sec = sec - hour * 3600 - min * 60;
        if (hour == 0) {
            return String.format("%02d:%02d", min, sec);
        }
        return String.format("%d:%02d:%02d", hour, min, sec);
    }

    public String getMoves(String nameWithProperties) {
        return getFileProp(nameWithProperties)[8];
    }

    public String setNbMoves(String nameWithProperties, int moves) {
        return setProperty(nameWithProperties, Integer.toString(moves), 8);
    }

    public String setEditable(String nameWithProperties, boolean editable) {
        return setProperty(nameWithProperties, "E", 10);
    }

    public boolean isEditable(String nameWithProperties) {
        String prop = getFileProp(nameWithProperties)[10];
        return "E".equals(prop);
    }

    public boolean isSolved(String nameWithProperties) {
        String prop = getFileProp(nameWithProperties)[11];
        if (Integer.parseInt(prop) <= 0) {
            return false;
        } else {
            return true;
        }
    }

    String setSolved(String nameWithProperties, boolean solved) {
        return setProperty(nameWithProperties, solved ? "1" : "0", 11);
    }


    /**
     * save/read level maps
     */
    public void saveMap() {
        try {
            String datFile = this.getEvtFolder() + MAP_DAT;
            File mapFile = new File(datFile);
            FileOutputStream fos = new FileOutputStream(mapFile);
            ObjectOutputStream oos = new ObjectOutputStream(fos);

            oos.writeObject(this.localLevelMap);
            oos.writeObject(this.webLevelMap);

            oos.flush();
            oos.close();
            fos.close();
        } catch (Exception e) {
        }
    }

    public void readMap() {
        //read from file
        try {
            String datFile = this.getEvtFolder() + MAP_DAT;
            File mapFile = new File(datFile);
            if (mapFile.exists()) {
                FileInputStream fis = new FileInputStream(mapFile);
                ObjectInputStream ois = new ObjectInputStream(fis);

                this.localLevelMap = (HashMap<String, String>) ois.readObject();
                this.webLevelMap = (HashMap<String, String>) ois.readObject();

                ois.close();
                fis.close();

            } else {
                this.saveMap();
            }
        } catch (Exception e) {
        }
    }

}

